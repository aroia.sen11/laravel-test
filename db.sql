/*
SQLyog Community
MySQL - 10.1.32-MariaDB : Database - encrypt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`encrypt` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `encrypt`;

/*Table structure for table `activations` */

CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activations_user_id_index` (`user_id`),
  CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `activations` */

/*Table structure for table `laravel2step` */

CREATE TABLE `laravel2step` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `authCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authCount` int(11) NOT NULL,
  `authStatus` tinyint(1) NOT NULL DEFAULT '0',
  `authDate` datetime DEFAULT NULL,
  `requestDate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `laravel2step_userid_index` (`userId`),
  CONSTRAINT `laravel2step_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `laravel2step` */

/*Table structure for table `laravel_logger_activity` */

CREATE TABLE `laravel_logger_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `userType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `route` longtext COLLATE utf8mb4_unicode_ci,
  `ipAddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userAgent` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` longtext COLLATE utf8mb4_unicode_ci,
  `methodType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `laravel_logger_activity` */

insert  into `laravel_logger_activity`(`id`,`description`,`userType`,`userId`,`route`,`ipAddress`,`userAgent`,`locale`,`referer`,`methodType`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Viewed social/redirect/twitch','Guest',NULL,'http://127.0.0.1:8000/social/redirect/twitch','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-21 03:22:58','2018-12-21 03:22:58',NULL),
(2,'Logged In','Registered',10,'http://127.0.0.1:8000/register','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','POST','2018-12-21 16:26:10','2018-12-21 16:26:10',NULL),
(3,'Viewed activate','Registered',10,'http://127.0.0.1:8000/activate','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-21 16:26:10','2018-12-21 16:26:10',NULL),
(4,'Viewed activation-required','Registered',10,'http://127.0.0.1:8000/activation-required','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/activate','GET','2018-12-21 16:26:57','2018-12-21 16:26:57',NULL),
(5,'Viewed activate/bMQdyfYeL6RGK2wkMfp8iSwqYb0ktwJgLrlRRni3gfU06921QQitvJ9wcUzrpAJI','Registered',10,'http://127.0.0.1:8000/activate/bMQdyfYeL6RGK2wkMfp8iSwqYb0ktwJgLrlRRni3gfU06921QQitvJ9wcUzrpAJI','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-21 16:27:10','2018-12-21 16:27:10',NULL),
(6,'Viewed home','Registered',10,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-21 16:27:11','2018-12-21 16:27:11',NULL),
(7,'Viewed profile/Darkhorse','Registered',10,'http://127.0.0.1:8000/profile/Darkhorse','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-21 16:27:26','2018-12-21 16:27:26',NULL),
(8,'Viewed profile/Darkhorse/edit','Registered',10,'http://127.0.0.1:8000/profile/Darkhorse/edit','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/profile/Darkhorse','GET','2018-12-21 16:27:38','2018-12-21 16:27:38',NULL),
(9,'Created avatar/upload','Registered',10,'http://127.0.0.1:8000/avatar/upload','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/profile/Darkhorse/edit','POST','2018-12-21 16:28:00','2018-12-21 16:28:00',NULL),
(10,'Viewed images/profile/10/avatar/avatar.jpg','Registered',10,'http://127.0.0.1:8000/images/profile/10/avatar/avatar.jpg?1545409681433=','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/profile/Darkhorse/edit','GET','2018-12-21 16:28:01','2018-12-21 16:28:01',NULL),
(11,'Logged Out','Registered',10,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/profile/Darkhorse/edit','POST','2018-12-21 16:28:29','2018-12-21 16:28:29',NULL),
(12,'Logged In','Registered',10,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-21 16:28:47','2018-12-21 16:28:47',NULL),
(13,'Viewed home','Registered',10,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-21 16:28:48','2018-12-21 16:28:48',NULL),
(14,'Logged Out','Registered',10,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-21 16:28:54','2018-12-21 16:28:54',NULL),
(15,'Reset Password','Guest',NULL,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/14f3fb5f45c21ab36babed0954c95c026169ef861eb40c55e5749ca9a1d9a3b1','POST','2018-12-21 16:56:16','2018-12-21 16:56:16',NULL),
(16,'Logged In','Registered',10,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/14f3fb5f45c21ab36babed0954c95c026169ef861eb40c55e5749ca9a1d9a3b1','POST','2018-12-21 16:56:16','2018-12-21 16:56:16',NULL),
(17,'Viewed home','Registered',10,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/14f3fb5f45c21ab36babed0954c95c026169ef861eb40c55e5749ca9a1d9a3b1','GET','2018-12-21 16:56:16','2018-12-21 16:56:16',NULL),
(18,'Logged Out','Registered',10,'http://localhost:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/home','POST','2018-12-21 16:56:27','2018-12-21 16:56:27',NULL),
(19,'Failed Login Attempt','Guest',NULL,'http://localhost:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/login','POST','2018-12-21 16:56:34','2018-12-21 16:56:34',NULL),
(20,'Logged In','Registered',10,'http://localhost:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/login','POST','2018-12-21 16:56:42','2018-12-21 16:56:42',NULL),
(21,'Viewed home','Registered',10,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/login','GET','2018-12-21 16:56:43','2018-12-21 16:56:43',NULL),
(22,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-22 02:24:33','2018-12-22 02:24:33',NULL),
(23,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-22 02:24:44','2018-12-22 02:24:44',NULL),
(24,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-22 02:24:55','2018-12-22 02:24:55',NULL),
(25,'Viewed social/redirect/37signals','Guest',NULL,'http://127.0.0.1:8000/social/redirect/37signals','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-22 02:25:05','2018-12-22 02:25:05',NULL),
(26,'Logged In','Registered',11,'http://127.0.0.1:8000/register','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','POST','2018-12-24 11:10:51','2018-12-24 11:10:51',NULL),
(27,'Viewed activate','Registered',11,'http://127.0.0.1:8000/activate','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-24 11:10:51','2018-12-24 11:10:51',NULL),
(28,'Viewed activation','Registered',11,'http://127.0.0.1:8000/activation','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/activate','GET','2018-12-24 11:11:10','2018-12-24 11:11:10',NULL),
(29,'Viewed activation-required','Registered',11,'http://127.0.0.1:8000/activation-required','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/activate','GET','2018-12-24 11:11:13','2018-12-24 11:11:13',NULL),
(30,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:14:30','2018-12-24 11:14:30',NULL),
(31,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:14:58','2018-12-24 11:14:58',NULL),
(32,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:15:10','2018-12-24 11:15:10',NULL),
(33,'Viewed activation-required','Registered',11,'http://127.0.0.1:8000/activation-required','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:15:56','2018-12-24 11:15:56',NULL),
(34,'Failed Login Attempt','Guest',NULL,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:16:21','2018-12-24 11:16:21',NULL),
(35,'Logged In','Registered',12,'http://127.0.0.1:8000/register','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','POST','2018-12-24 11:17:29','2018-12-24 11:17:29',NULL),
(36,'Viewed activate','Registered',12,'http://127.0.0.1:8000/activate','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-24 11:17:30','2018-12-24 11:17:30',NULL),
(37,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:18:09','2018-12-24 11:18:09',NULL),
(38,'Viewed activate/qdy5Pf1Bo6PgopcEUI2oO5RKUtMuvQBmEtVPLuXSLUocFnklzBoCxKehXLWnzfxl','Registered',12,'http://127.0.0.1:8000/activate/qdy5Pf1Bo6PgopcEUI2oO5RKUtMuvQBmEtVPLuXSLUocFnklzBoCxKehXLWnzfxl','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-24 11:18:10','2018-12-24 11:18:10',NULL),
(39,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-24 11:18:11','2018-12-24 11:18:11',NULL),
(40,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-24 11:22:21','2018-12-24 11:22:21',NULL),
(41,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-24 11:23:05','2018-12-24 11:23:05',NULL),
(42,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:28:27','2018-12-24 11:28:27',NULL),
(43,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:28:30','2018-12-24 11:28:30',NULL),
(44,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:28:37','2018-12-24 11:28:37',NULL),
(45,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:35:35','2018-12-24 11:35:35',NULL),
(46,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:35:37','2018-12-24 11:35:37',NULL),
(47,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:35:40','2018-12-24 11:35:40',NULL),
(48,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:35:50','2018-12-24 11:35:50',NULL),
(49,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:35:54','2018-12-24 11:35:54',NULL),
(50,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-24 11:37:55','2018-12-24 11:37:55',NULL),
(51,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-24 11:37:55','2018-12-24 11:37:55',NULL),
(52,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:37:59','2018-12-24 11:37:59',NULL),
(53,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:38:05','2018-12-24 11:38:05',NULL),
(54,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:40:13','2018-12-24 11:40:13',NULL),
(55,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:40:22','2018-12-24 11:40:22',NULL),
(56,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:40:24','2018-12-24 11:40:24',NULL),
(57,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:40:34','2018-12-24 11:40:34',NULL),
(58,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:40:41','2018-12-24 11:40:41',NULL),
(59,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:42:28','2018-12-24 11:42:28',NULL),
(60,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:42:50','2018-12-24 11:42:50',NULL),
(61,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:43:31','2018-12-24 11:43:31',NULL),
(62,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:43:33','2018-12-24 11:43:33',NULL),
(63,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:43:36','2018-12-24 11:43:36',NULL),
(64,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-24 11:47:44','2018-12-24 11:47:44',NULL),
(65,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-24 11:48:03','2018-12-24 11:48:03',NULL),
(66,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 10:00:31','2018-12-28 10:00:31',NULL),
(67,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 10:00:31','2018-12-28 10:00:31',NULL),
(68,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:00:39','2018-12-28 10:00:39',NULL),
(69,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:00:49','2018-12-28 10:00:49',NULL),
(70,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:05:21','2018-12-28 10:05:21',NULL),
(71,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:05:25','2018-12-28 10:05:25',NULL),
(72,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:05:27','2018-12-28 10:05:27',NULL),
(73,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:05:28','2018-12-28 10:05:28',NULL),
(74,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:06:12','2018-12-28 10:06:12',NULL),
(75,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 10:09:15','2018-12-28 10:09:15',NULL),
(76,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 10:09:16','2018-12-28 10:09:16',NULL),
(77,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:09:20','2018-12-28 10:09:20',NULL),
(78,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:13:56','2018-12-28 10:13:56',NULL),
(79,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:13:59','2018-12-28 10:13:59',NULL),
(80,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:14:01','2018-12-28 10:14:01',NULL),
(81,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:14:03','2018-12-28 10:14:03',NULL),
(82,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:14:06','2018-12-28 10:14:06',NULL),
(83,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:14:08','2018-12-28 10:14:08',NULL),
(84,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:14:10','2018-12-28 10:14:10',NULL),
(85,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 10:23:35','2018-12-28 10:23:35',NULL),
(86,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 10:23:35','2018-12-28 10:23:35',NULL),
(87,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:23:44','2018-12-28 10:23:44',NULL),
(88,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','GET','2018-12-28 10:24:20','2018-12-28 10:24:20',NULL),
(89,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 15:17:43','2018-12-28 15:17:43',NULL),
(90,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:17:43','2018-12-28 15:17:43',NULL),
(91,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:27:45','2018-12-28 15:27:45',NULL),
(92,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:28:05','2018-12-28 15:28:05',NULL),
(93,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 15:28:07','2018-12-28 15:28:07',NULL),
(94,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:28:38','2018-12-28 15:28:38',NULL),
(95,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 15:28:41','2018-12-28 15:28:41',NULL),
(96,'Logged Out','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 15:28:41','2018-12-28 15:28:41',NULL),
(97,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 15:28:54','2018-12-28 15:28:54',NULL),
(98,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:28:54','2018-12-28 15:28:54',NULL),
(99,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 15:40:38','2018-12-28 15:40:38',NULL),
(100,'Logged Out','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 15:40:38','2018-12-28 15:40:38',NULL),
(101,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 15:43:33','2018-12-28 15:43:33',NULL),
(102,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:43:34','2018-12-28 15:43:34',NULL),
(103,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:51:39','2018-12-28 15:51:39',NULL),
(104,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:56:31','2018-12-28 15:56:31',NULL),
(105,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 15:56:51','2018-12-28 15:56:51',NULL),
(106,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:03:01','2018-12-28 16:03:01',NULL),
(107,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:03:57','2018-12-28 16:03:57',NULL),
(108,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:05:18','2018-12-28 16:05:18',NULL),
(109,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:05:32','2018-12-28 16:05:32',NULL),
(110,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:05:49','2018-12-28 16:05:49',NULL),
(111,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:06:19','2018-12-28 16:06:19',NULL),
(112,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:06:40','2018-12-28 16:06:40',NULL),
(113,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:06:52','2018-12-28 16:06:52',NULL),
(114,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:07:17','2018-12-28 16:07:17',NULL),
(115,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:08:37','2018-12-28 16:08:37',NULL),
(116,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:10:48','2018-12-28 16:10:48',NULL),
(117,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:13:00','2018-12-28 16:13:00',NULL),
(118,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:15:25','2018-12-28 16:15:25',NULL),
(119,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:17:37','2018-12-28 16:17:37',NULL),
(120,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:17:40','2018-12-28 16:17:40',NULL),
(121,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:17:59','2018-12-28 16:17:59',NULL),
(122,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:18:40','2018-12-28 16:18:40',NULL),
(123,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:18:42','2018-12-28 16:18:42',NULL),
(124,'Logged Out','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:18:43','2018-12-28 16:18:43',NULL),
(125,'Logged In','Registered',12,'http://127.0.0.1:8000/login','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','POST','2018-12-28 16:21:58','2018-12-28 16:21:58',NULL),
(126,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/login','GET','2018-12-28 16:21:58','2018-12-28 16:21:58',NULL),
(127,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','POST','2018-12-28 16:26:02','2018-12-28 16:26:02',NULL),
(128,'Logged Out','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','POST','2018-12-28 16:26:03','2018-12-28 16:26:03',NULL),
(129,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-28 16:41:35','2018-12-28 16:41:35',NULL),
(130,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAnY6F-I23A8aaC1okrv8SbMOX0UeRPZWzwT_-3t2WVS7E6oKLzh0iO1NCTMNwBut6U19ZSyOLqEJwdTHpfWDw&prompt=consent&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=806cbc592390b314dcd9dad597ad27b62dacbe4c..36e7&state=KtIAN8cmSGvwuBaH74I5q6xbDlVBInmMymV8gRav','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:41:57','2018-12-28 16:41:57',NULL),
(131,'Logged In','Registered',13,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAnY6F-I23A8aaC1okrv8SbMOX0UeRPZWzwT_-3t2WVS7E6oKLzh0iO1NCTMNwBut6U19ZSyOLqEJwdTHpfWDw&prompt=consent&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=806cbc592390b314dcd9dad597ad27b62dacbe4c..36e7&state=KtIAN8cmSGvwuBaH74I5q6xbDlVBInmMymV8gRav','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:42:01','2018-12-28 16:42:01',NULL),
(132,'Viewed home','Registered',13,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:42:01','2018-12-28 16:42:01',NULL),
(133,'Created logout','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:44:00','2018-12-28 16:44:00',NULL),
(134,'Logged Out','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:44:00','2018-12-28 16:44:00',NULL),
(135,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:44:34','2018-12-28 16:44:34',NULL),
(136,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAw0aNCCcxlH2vZ4-jec-Jg4yvc34TkS4I8qFEcgY2LVEZbkvl5rT0CY3bClAKeqTG3eJtY8m8SPVtFU5zExDc&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&session_state=79be8e3bd039f5b007d65152eb57b946eb19e209..7e8c&state=VeHG1k8yatk8nkgcGDzyuer5rfq2C6zoU7VyzEU3','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:44:34','2018-12-28 16:44:34',NULL),
(137,'Logged In','Registered',13,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAw0aNCCcxlH2vZ4-jec-Jg4yvc34TkS4I8qFEcgY2LVEZbkvl5rT0CY3bClAKeqTG3eJtY8m8SPVtFU5zExDc&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&session_state=79be8e3bd039f5b007d65152eb57b946eb19e209..7e8c&state=VeHG1k8yatk8nkgcGDzyuer5rfq2C6zoU7VyzEU3','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:44:36','2018-12-28 16:44:36',NULL),
(138,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:47:48','2018-12-28 16:47:48',NULL),
(139,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAg1EwNMdeQofZlaXdD-hMk1rjvRNfHLpXa2hAnqsZM-l2AKYsw5JfkvXlrv96AbeMajOmLzckUhMUfi16EVus&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=2b297e4207334168ed44ebaf8b20edccab3addac..747d&state=slHN2vEuACdHKHSoasaZ7RqGHNGMCbSjYewDxy5D','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:47:48','2018-12-28 16:47:48',NULL),
(140,'Logged In','Registered',13,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQAg1EwNMdeQofZlaXdD-hMk1rjvRNfHLpXa2hAnqsZM-l2AKYsw5JfkvXlrv96AbeMajOmLzckUhMUfi16EVus&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=2b297e4207334168ed44ebaf8b20edccab3addac..747d&state=slHN2vEuACdHKHSoasaZ7RqGHNGMCbSjYewDxy5D','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:47:50','2018-12-28 16:47:50',NULL),
(141,'Viewed home','Registered',13,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:47:51','2018-12-28 16:47:51',NULL),
(142,'Created logout','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:47:57','2018-12-28 16:47:57',NULL),
(143,'Logged Out','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:47:57','2018-12-28 16:47:57',NULL),
(144,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:48:01','2018-12-28 16:48:01',NULL),
(145,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQCvxfnaX2HyWI2SEuc39VT1fSUcHzqIzPnliInb8mCe7T3meGtM6MNZOmtBcJRleM5VGVprZ_SK9FozXy1_Rqc&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=42665a0d297f27236639526e4a9b64b9e34adcc8..a630&state=sj7eHs0i7eDSEccOmIkMuBQSkEbasGUXBEFHZFvN','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:48:02','2018-12-28 16:48:02',NULL),
(146,'Logged In','Registered',13,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQCvxfnaX2HyWI2SEuc39VT1fSUcHzqIzPnliInb8mCe7T3meGtM6MNZOmtBcJRleM5VGVprZ_SK9FozXy1_Rqc&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=42665a0d297f27236639526e4a9b64b9e34adcc8..a630&state=sj7eHs0i7eDSEccOmIkMuBQSkEbasGUXBEFHZFvN','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:48:03','2018-12-28 16:48:03',NULL),
(147,'Viewed home','Registered',13,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:48:04','2018-12-28 16:48:04',NULL),
(148,'Created logout','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:48:08','2018-12-28 16:48:08',NULL),
(149,'Logged Out','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 16:48:09','2018-12-28 16:48:09',NULL),
(150,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 16:50:19','2018-12-28 16:50:19',NULL),
(151,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=1&code=4%2FwQD0VsJrQ6HmJpDhb1_9LP3q76LUkLHdnWh6bhFCsqxBvq4cpJ5IqTqCWvvxGF48cjbyWRtuvuoK6H99WWUFSM0&prompt=consent&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=7e00d283ae7b557fd6e5baea4a3be688eb4b83bd..0257&state=4eRMdkB1fPVDtuduTPArt3S1lb8qQju2UMt5JbXC','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:50:25','2018-12-28 16:50:25',NULL),
(152,'Logged In','Registered',12,'http://127.0.0.1:8000/social/handle/google?authuser=1&code=4%2FwQD0VsJrQ6HmJpDhb1_9LP3q76LUkLHdnWh6bhFCsqxBvq4cpJ5IqTqCWvvxGF48cjbyWRtuvuoK6H99WWUFSM0&prompt=consent&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&session_state=7e00d283ae7b557fd6e5baea4a3be688eb4b83bd..0257&state=4eRMdkB1fPVDtuduTPArt3S1lb8qQju2UMt5JbXC','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:50:29','2018-12-28 16:50:29',NULL),
(153,'Viewed home','Registered',12,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 16:50:29','2018-12-28 16:50:29',NULL),
(154,'Created logout','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:25:35','2018-12-28 17:25:35',NULL),
(155,'Logged Out','Registered',12,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:25:36','2018-12-28 17:25:36',NULL),
(156,'Reset Password','Guest',NULL,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/abdc441addfdb3b9f1feee02e976d2aa4ee087ce2f69887eed6950a975247b70','POST','2018-12-28 17:33:42','2018-12-28 17:33:42',NULL),
(157,'Logged In','Registered',13,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/abdc441addfdb3b9f1feee02e976d2aa4ee087ce2f69887eed6950a975247b70','POST','2018-12-28 17:33:42','2018-12-28 17:33:42',NULL),
(158,'Viewed home','Registered',13,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/abdc441addfdb3b9f1feee02e976d2aa4ee087ce2f69887eed6950a975247b70','GET','2018-12-28 17:33:42','2018-12-28 17:33:42',NULL),
(159,'Viewed home','Registered',13,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:35:46','2018-12-28 17:35:46',NULL),
(160,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-28 17:37:15','2018-12-28 17:37:15',NULL),
(161,'Viewed social/redirect/google','Guest',NULL,'http://127.0.0.1:8000/social/redirect/google','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/','GET','2018-12-28 17:47:41','2018-12-28 17:47:41',NULL),
(162,'Viewed social/handle/google','Guest',NULL,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQC5nLXwo_we-v4JSIXLMROBzTaUln4r0G2pw3xXF75NLMwzKsVSfiMWdDALn5YjbjtAM9cHehUhT5CeFbYbBcw&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&session_state=e38ca709691bbd3dd39fda7690ebdfd0431d16f6..63eb&state=JubaV5GxsZC8uMtOzWtjCTweXeIKIuRLvo0si3Yb','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:47:45','2018-12-28 17:47:45',NULL),
(163,'Logged In','Registered',13,'http://127.0.0.1:8000/social/handle/google?authuser=0&code=4%2FwQC5nLXwo_we-v4JSIXLMROBzTaUln4r0G2pw3xXF75NLMwzKsVSfiMWdDALn5YjbjtAM9cHehUhT5CeFbYbBcw&prompt=none&scope=openid%20email%20profile%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&session_state=e38ca709691bbd3dd39fda7690ebdfd0431d16f6..63eb&state=JubaV5GxsZC8uMtOzWtjCTweXeIKIuRLvo0si3Yb','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:47:47','2018-12-28 17:47:47',NULL),
(164,'Viewed home','Registered',13,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:47:47','2018-12-28 17:47:47',NULL),
(165,'Created logout','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:47:54','2018-12-28 17:47:54',NULL),
(166,'Logged Out','Registered',13,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:47:54','2018-12-28 17:47:54',NULL),
(167,'Logged In','Registered',14,'http://127.0.0.1:8000/register','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','POST','2018-12-28 17:50:06','2018-12-28 17:50:06',NULL),
(168,'Viewed activate','Registered',14,'http://127.0.0.1:8000/activate','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-28 17:50:07','2018-12-28 17:50:07',NULL),
(169,'Viewed activate/KAWfm3Q8SuWciPQuyvJisjnsr7SJn4OkqQTgt767jXY1D5ijSoFXJqEl7MK6GdlF','Registered',14,'http://127.0.0.1:8000/activate/KAWfm3Q8SuWciPQuyvJisjnsr7SJn4OkqQTgt767jXY1D5ijSoFXJqEl7MK6GdlF','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:51:51','2018-12-28 17:51:51',NULL),
(170,'Viewed home','Registered',14,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:51:52','2018-12-28 17:51:52',NULL),
(171,'Viewed activate','Registered',14,'http://127.0.0.1:8000/activate','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-28 17:51:59','2018-12-28 17:51:59',NULL),
(172,'Viewed home','Registered',14,'http://127.0.0.1:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/register','GET','2018-12-28 17:52:00','2018-12-28 17:52:00',NULL),
(173,'Created logout','Registered',14,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:52:31','2018-12-28 17:52:31',NULL),
(174,'Logged Out','Registered',14,'http://127.0.0.1:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://127.0.0.1:8000/home','POST','2018-12-28 17:52:31','2018-12-28 17:52:31',NULL),
(175,'Viewed home','Registered',13,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9',NULL,'GET','2018-12-28 17:52:57','2018-12-28 17:52:57',NULL),
(176,'Created logout','Registered',13,'http://localhost:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/home','POST','2018-12-28 17:53:02','2018-12-28 17:53:02',NULL),
(177,'Logged Out','Registered',13,'http://localhost:8000/logout','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/home','POST','2018-12-28 17:53:02','2018-12-28 17:53:02',NULL),
(178,'Reset Password','Guest',NULL,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/86950ad0ac8a6bbb8fda9dcce89b6088f8c7b8d6fdd01592ca645c687a8e1fe5','POST','2018-12-28 17:55:20','2018-12-28 17:55:20',NULL),
(179,'Logged In','Registered',14,'http://localhost:8000/password/reset','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/86950ad0ac8a6bbb8fda9dcce89b6088f8c7b8d6fdd01592ca645c687a8e1fe5','POST','2018-12-28 17:55:20','2018-12-28 17:55:20',NULL),
(180,'Viewed home','Registered',14,'http://localhost:8000/home','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','en-US,en;q=0.9','http://localhost:8000/password/reset/86950ad0ac8a6bbb8fda9dcce89b6088f8c7b8d6fdd01592ca645c687a8e1fe5','GET','2018-12-28 17:55:20','2018-12-28 17:55:20',NULL);

/*Table structure for table `migrations` */

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2016_01_15_105324_create_roles_table',1),
(4,'2016_01_15_114412_create_role_user_table',1),
(5,'2016_01_26_115212_create_permissions_table',1),
(6,'2016_01_26_115523_create_permission_role_table',1),
(7,'2016_02_09_132439_create_permission_user_table',1),
(8,'2017_03_09_082449_create_social_logins_table',1),
(9,'2017_03_09_082526_create_activations_table',1),
(10,'2017_03_20_213554_create_themes_table',1),
(11,'2017_03_21_042918_create_profiles_table',1),
(12,'2017_11_04_103444_create_laravel_logger_activity_table',1),
(13,'2017_12_09_070937_create_two_step_auth_table',1);

/*Table structure for table `password_resets` */

CREATE TABLE `password_resets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

insert  into `password_resets`(`id`,`email`,`token`,`created_at`) values 
(2,'alexander950223@gmail.com','$2y$10$UOKlfqXJwaVjmm8By/3MnuNv06.lHgC/52MOcB9RpRqvlaw7tBUDa','2018-12-28 17:35:31');

/*Table structure for table `permission_role` */

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`id`,`permission_id`,`role_id`,`created_at`,`updated_at`) values 
(1,1,1,'2018-12-21 01:22:13','2018-12-21 01:22:13'),
(2,2,1,'2018-12-21 01:22:13','2018-12-21 01:22:13'),
(3,3,1,'2018-12-21 01:22:13','2018-12-21 01:22:13'),
(4,4,1,'2018-12-21 01:22:13','2018-12-21 01:22:13');

/*Table structure for table `permission_user` */

CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_user` */

/*Table structure for table `permissions` */

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`slug`,`description`,`model`,`created_at`,`updated_at`) values 
(1,'Can View Users','view.users','Can view users','Permission','2018-12-21 01:22:12','2018-12-21 01:22:12'),
(2,'Can Create Users','create.users','Can create new users','Permission','2018-12-21 01:22:12','2018-12-21 01:22:12'),
(3,'Can Edit Users','edit.users','Can edit users','Permission','2018-12-21 01:22:12','2018-12-21 01:22:12'),
(4,'Can Delete Users','delete.users','Can delete users','Permission','2018-12-21 01:22:12','2018-12-21 01:22:12');

/*Table structure for table `profiles` */

CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `theme_id` int(10) unsigned NOT NULL DEFAULT '1',
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `twitter_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `github_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_theme_id_foreign` (`theme_id`),
  KEY `profiles_user_id_index` (`user_id`),
  CONSTRAINT `profiles_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `profiles` */

insert  into `profiles`(`id`,`user_id`,`theme_id`,`location`,`bio`,`twitter_username`,`github_username`,`avatar`,`avatar_status`,`created_at`,`updated_at`) values 
(1,1,1,NULL,NULL,NULL,NULL,NULL,0,'2018-12-21 01:22:15','2018-12-21 01:22:15'),
(2,2,1,NULL,NULL,NULL,NULL,NULL,0,'2018-12-21 01:22:15','2018-12-21 01:22:15'),
(4,13,1,NULL,NULL,NULL,NULL,NULL,0,'2018-12-28 16:42:01','2018-12-28 16:42:01'),
(5,14,1,NULL,NULL,NULL,NULL,NULL,0,'2018-12-28 17:51:51','2018-12-28 17:51:51');

/*Table structure for table `role_user` */

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`id`,`role_id`,`user_id`,`created_at`,`updated_at`) values 
(1,1,1,'2018-12-21 01:22:15','2018-12-21 01:22:15'),
(2,2,2,'2018-12-21 01:22:15','2018-12-21 01:22:15'),
(6,2,13,'2018-12-28 16:42:01','2018-12-28 16:42:01'),
(8,2,14,'2018-12-28 17:51:51','2018-12-28 17:51:51');

/*Table structure for table `roles` */

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`slug`,`description`,`level`,`created_at`,`updated_at`) values 
(1,'Admin','admin','Admin Role',5,'2018-12-21 01:22:13','2018-12-21 01:22:13'),
(2,'User','user','User Role',1,'2018-12-21 01:22:13','2018-12-21 01:22:13'),
(3,'Unverified','unverified','Unverified Role',0,'2018-12-21 01:22:13','2018-12-21 01:22:13');

/*Table structure for table `social_logins` */

CREATE TABLE `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_index` (`user_id`),
  CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `social_logins` */

insert  into `social_logins`(`id`,`user_id`,`provider`,`social_id`,`created_at`,`updated_at`) values 
(1,13,'google','100181473684010929226','2018-12-28 16:42:01','2018-12-28 16:42:01');

/*Table structure for table `themes` */

CREATE TABLE `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taggable_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `themes_name_unique` (`name`),
  UNIQUE KEY `themes_link_unique` (`link`),
  KEY `themes_taggable_type_taggable_id_index` (`taggable_type`,`taggable_id`),
  KEY `themes_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `themes` */

insert  into `themes`(`id`,`name`,`link`,`notes`,`status`,`taggable_type`,`taggable_id`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Default','null',NULL,1,'theme',1,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(2,'Darkly','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/darkly/bootstrap.min.css',NULL,1,'theme',2,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(3,'Cyborg','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cyborg/bootstrap.min.css',NULL,1,'theme',3,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(4,'Cosmo','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cosmo/bootstrap.min.css',NULL,1,'theme',4,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(5,'Cerulean','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/cerulean/bootstrap.min.css',NULL,1,'theme',5,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(6,'Flatly','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/flatly/bootstrap.min.css',NULL,1,'theme',6,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(7,'Journal','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/journal/bootstrap.min.css',NULL,1,'theme',7,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(8,'Lumen','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/lumen/bootstrap.min.css',NULL,1,'theme',8,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(9,'Paper','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/paper/bootstrap.min.css',NULL,1,'theme',9,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(10,'Readable','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/readable/bootstrap.min.css',NULL,1,'theme',10,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(11,'Sandstone','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/sandstone/bootstrap.min.css',NULL,1,'theme',11,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(12,'Simplex','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/simplex/bootstrap.min.css',NULL,1,'theme',12,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(13,'Slate','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/slate/bootstrap.min.css',NULL,1,'theme',13,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(14,'Spacelab','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/spacelab/bootstrap.min.css',NULL,1,'theme',14,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(15,'Superhero','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/superhero/bootstrap.min.css',NULL,1,'theme',15,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(16,'United','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/united/bootstrap.min.css',NULL,1,'theme',16,'2018-12-21 01:22:13','2018-12-21 01:22:14',NULL),
(17,'Yeti','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/yeti/bootstrap.min.css',NULL,1,'theme',17,'2018-12-21 01:22:13','2018-12-21 01:22:15',NULL),
(18,'Bootstrap 4.0.0 Alpha','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css',NULL,1,'theme',18,'2018-12-21 01:22:13','2018-12-21 01:22:15',NULL),
(19,'Materialize','https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css',NULL,1,'theme',19,'2018-12-21 01:22:13','2018-12-21 01:22:15',NULL),
(20,'Bootstrap Material Design 4.0.0','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.0/bootstrap-material-design.min.css',NULL,1,'theme',20,'2018-12-21 01:22:13','2018-12-21 01:22:15',NULL),
(21,'Bootstrap Material Design 4.0.2','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.2/bootstrap-material-design.min.css',NULL,1,'theme',21,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(22,'mdbootstrap','https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.1/css/mdb.min.css',NULL,1,'theme',22,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(23,'Litera','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/litera/bootstrap.min.css',NULL,1,'theme',23,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(24,'Lux','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/lux/bootstrap.min.css',NULL,1,'theme',24,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(25,'Materia','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/materia/bootstrap.min.css',NULL,1,'theme',25,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(26,'Minty','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/minty/bootstrap.min.css',NULL,1,'theme',26,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(27,'Pulse','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/pulse/bootstrap.min.css',NULL,1,'theme',27,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(28,'Sketchy','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/sketchy/bootstrap.min.css',NULL,1,'theme',28,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL),
(29,'Solar','https://maxcdn.bootstrapcdn.com/bootswatch/4.0.0/solar/bootstrap.min.css',NULL,1,'theme',29,'2018-12-21 01:22:14','2018-12-21 01:22:15',NULL);

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signup_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_confirmation_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_sm_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_name_unique` (`name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`first_name`,`last_name`,`email`,`password`,`remember_token`,`activated`,`token`,`signup_ip_address`,`signup_confirmation_ip_address`,`signup_sm_ip_address`,`admin_ip_address`,`updated_ip_address`,`deleted_ip_address`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'savanna70','Naomi','Kris','admin@admin.com','$2y$10$A95ju892zeBjvVcctSk9hOtN97AN8ljDrDF0oQVf4vpts1lMZU8IO',NULL,1,'jlYlFzUx5i9ri9Z5qWHFzIsbWgUkqKvlzvq13a09ZI0lEikzYKXQwDa5wbWUYZ6O',NULL,'156.45.123.215',NULL,'176.247.233.184',NULL,NULL,'2018-12-21 01:22:15','2018-12-21 01:22:15',NULL),
(2,'halvorson.maude','Charlie','Witting','user@user.com','$2y$10$woW3A52E8rR/e0YQth9hYeVakokFR/dWdG44OcBOovY9xp4EhpGYm',NULL,1,'bZaDuZm4vkFsh6Oz6yKINE8mRbV9f5zordxzM2MMep5gImq7te6ZR5DJdgAo4K4A','39.18.43.109','170.20.240.227',NULL,NULL,NULL,NULL,'2018-12-21 01:22:15','2018-12-21 01:22:15',NULL),
(13,'AlexanderIvanov','Alexander','Ivanov','alexander950223@gmail.com','$2y$10$pfLgyUjcOSidUZ7IQseZBuA2wlDlx/GVRdNpckE4ZfXg5nVXRv4yW','yKJfOlP6ziE0bggEevhp7sw6UEZ69hhl60oRR1Fn4tpXxzPJfVqeKU4F8mmB',1,'6Q7SjxB2v0fTJjXmlfNX3pzwownMCv0vx8hVejoU18bcm2LMZQ5xrxsk7R09cjuY',NULL,NULL,'0.0.0.0',NULL,NULL,NULL,'2018-12-28 16:42:01','2018-12-28 17:33:42',NULL),
(14,'Darkhorse0223','Alexander','Pete','blackcat20180000@gmail.com','$2y$10$CWZeGGi50/ZaCytBa4ZkIOnDOXJec63hPOnzfk1vMpn4/wDA4qfZa','vzighXuRtjSGxbjl1xSQq3ATpIkWNHvAmwxfMpZEwlID1qd4HBhPpEYHOTIr',1,'dg6IKykkPabH1trieLEgP4B7VueNYoMrxrZG3mPoEdLqCkwln4Yn3CrXu4mmyxMh','0.0.0.0','0.0.0.0',NULL,NULL,NULL,NULL,'2018-12-28 17:50:02','2018-12-28 17:55:20',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
