@extends('layouts.app')

@section('content')

    <section class="jumbotron" style="background-color: #fff;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 centered">
                </div>
            </div>
        </div>

        <div style="text-align:center">
            <h3 class="jumbotron-heading customFadeInDown" style="color:#3a13ec;font-size:47px;font-weight:400;">
                Welcome {{Auth::user()->name}}
            </h3>
            <h2>Tabbed Image Gallery</h2>
            <p>Click on the images below:</p>
        </div>

        <!-- The four columns -->
        <div class="row">
            <div class="column">
                <img src="https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Nature" style="width:100%"  height="250px" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="https://timedotcom.files.wordpress.com/2015/07/earth-blue-marble-2002.jpg" alt="Snow" style="width:100%" height="250px" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="https://s3.amazonaws.com/images.seroundtable.com/google-css-images-1515761601.jpg" alt="Mountains" style="width:100%"  height="250px" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="https://www.presse-citron.net/wordpress_prod/wp-content/uploads/2018/11/meilleure-banque-image.jpg" alt="Lights" style="width:100%"  height="250px" onclick="myFunction(this);">
            </div>
        </div>

        <div class="container">
            <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
            <img id="expandedImg" style="width:100%">
            <div id="imgtext"></div>
        </div>

        <script>
            function myFunction(imgs) {
                var expandImg = document.getElementById("expandedImg");
                var imgText = document.getElementById("imgtext");
                expandImg.src = imgs.src;
                imgText.innerHTML = imgs.alt;
                expandImg.parentElement.style.display = "block";
            }
        </script>
    </section>

    <div class="footer footer--light">
        <div class="container">
            <div class="credits">
                <p>Copyright © 2019 SHMS </p>
            </div>
        </div>
    </div>

@endsection
