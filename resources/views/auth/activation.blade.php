@extends('layouts.app-js')

@section('content')



	<section class="home-section" id="home_wrapper">
		<!--begin container -->
		<div class="container">
			<!--begin row -->
			<div class="row">
				<!--begin col-md-7-->
				<div class="col-md-12 padding-top-20">
					<h1>Activation REquired!</h1>
				</div>
				<!--end col-md-7-->
			</div>
			<!--end row -->
		</div>

	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="card card-default">
					<div class="card-header">{{ trans('titles.activation') }}</div>
					<div class="card-body">
						<p>{{ trans('auth.regThanks') }}</p>
						<p>{{ trans('auth.anEmailWasSent',['email' => $email, 'date' => $date ] ) }}</p>
						<p>{{ trans('auth.clickInEmail') }}</p>
						<p><a href='/activation' class="btn btn-primary">{{ trans('auth.clickHereResend') }}</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
@endsection
