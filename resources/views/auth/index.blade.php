
@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('frontend/form.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/theme.min.css')}}" />        

<section class="jumbotron" style="background-color: #fff;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="jumbotron-heading customFadeInDown" style="color:#2e5bec;font-size:47px;font-weight:400;">Laravel-Auth</h1>
				<p class="sub-text customFadeInDown">
					 {{--Laravel-Auth is a Complete Build of Laravel 5.7 with Email Registration Verification, Social Authentication, User Roles and Permissions, and Profiles. Built on Bootstrap 4.--}}
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec suscipit ac nulla non consectetur. Fusce venenatis vulputate magna, ac lobortis sem pellentesque quis. Nullam varius a diam vel aliquet. Donec quam sapien, porta vitae mi sed, vulputate vestibulum velit. Nunc viverra gravida aliquam. Morbi luctus facilisis nibh, vitae maximus ante tincidunt ac. Aenean pulvinar odio massa, id facilisis augue dictum ullamcorper. Nulla cursus dui non nunc pulvinar efficitur. Vestibulum condimentum nisi ac magna euismod iaculis
				</p>

			</div>
			<div class="col-md-6"></div>
		</div>
	</div>
</section>

<div class="footer footer--light">
	<div class="container">
		<div class="credits">
			<p>Copyright © 2019 SHMS </p>
		</div>
	</div>
</div>
<script>
	function validateForm(){
	      pass =                $("#user_password").val();
	      con_pass =            $("$confirm_password");
	      if(pass != con_pass){
	        alert("Password No matched!")
	      }
	    }
</script>








@endsection