<nav class="navbar navbar-expand-lg navbar-light bg-light"> <a class="navbar-brand" href="<?php echo e(url('/')); ?>" style="color:#2e5bec;">SHMS DEMO</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active"> <a class="nav-link" href="<?php echo e(url('/')); ?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item"> <a class="nav-link" href="#">About <span class="sr-only">(current)</span></a>
            </li>
            </li>
            </li>
        </ul>
        <?php if(!Auth::check()): ?>
        <ul class="nav navbar-nav navbar-right ml-auto">
            <li class="nav-item" style="margin-right: 10px"> 
                <a href="<?php echo e(url('/login')); ?>" class="btn btn-primary get-started-btn mt-1 mb-1" style="background: #33cabb; border-color:#33cabb;">Login</a>
                
            </li>
            <li class="nav-item"> 
                <a href="<?php echo e(url('/register')); ?>" class="btn btn-primary get-started-btn mt-1 mb-1" style="background: #33cabb; border-color:#33cabb;">Sign up</a>
             </li>
        </ul>
        <?php else: ?>
        <ul class="nav navbar-nav navbar-right ml-auto">
            <li class="nav-item" style="margin-right: 10px"> 
                <span  class="nav-link">
                    <?php echo e(Auth::user()->name); ?>

                </span>
            </li>

            <li class="nav-item"> 
                

                
                    
                
            <a href="<?php echo e(route('logout')); ?>"  class="nav-link">Logout</a>

            </li>
        </ul>
        <?php endif; ?>
    </div>
</nav>