<?php $__env->startSection('content'); ?>



    <section class="home-section" id="home_wrapper">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-7-->
                <div class="col-md-12 padding-top-20">
                    <h1>Secure your files, Start Now!</h1>
                </div>
                <!--end col-md-7-->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </section>
    <!--end home section -->
    <!--begin section-white -->
    <section class="section-white">
        <div class="container">
            <div class="row">
                <div uk-grid>
                    <div class="uk-width-1-3@m">
                        <h3 class="uk-heading-divider">Step 1</h3>
                        <h4><span uk-icon="folder"></span> Select File to encrypt</h4>
                        <form>
                            <div class="uk-margin" uk-margin>
                                <div uk-form-custom="target: true">
                                    <input type="file">
                                    <input id="file" class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>



                                    
                                    


                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="uk-width-1-3@m">
                        <h3 class="uk-heading-divider">Step 2</h3>
                        <h4><span uk-icon="cog"></span> Set Policy</h4>
                        <form>
                            <!--
                                                        <div class="uk-margin">
                                                            <label class="uk-form-label" for="form-horizontal-text">Authorized Users:</label>
                                                            <div class="uk-form-controls">
                                                                <input class="uk-input" id="form-horizontal-text" type="text" placeholder="Recipient Email">
                                                            </div>
                                                        </div>
                            -->
                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-text">Authorized Users:</label><br>
                                <div uk-form-custom="target: true">



                                    <input type="email" id="recepientEmails" class="uk-input" placeholder="Recipient email">

                                    
                                </div>
                                <button class="uk-button uk-button-default">Add</button>
                            </div>
                            <div class="uk-margin">
                                <label class="uk-form-label" for="form-horizontal-text">Expiry:</label>
                                <div uk-form-custom="target: > * > span:first-child">

                                        <select class="form-control " id="expiry">
                                            <option value="600">10 Minutes</option>
                                            <option value="1800">30 Minutes</option>
                                            <option value="3600">1 Hour</option>
                                            <option value="10800">3 Hours</option>
                                            <option value="21600">6 Hours</option>
                                            <option value="43200">12 Hours</option>
                                            <option value="86400">1 Day</option>
                                            <option value="259200">3 Days</option>
                                            <option value="432000">5 Days</option>
                                            <option value="604800">1 Week</option>
                                            <option value="864000">10 Days</option>
                                            <option value="1209600">2 Weeks</option>
                                            <option value="2592000">1 Month</option>
                                            <option value="0" selected="selected">Never</option>


                                    </select>
                                    <button class="uk-button" type="button" tabindex="-1">
                                        <span></span>
                                        <span uk-icon="icon: chevron-down"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="uk-width-1-3@m">
                        <h3 class="uk-heading-divider">Step 3</h3>
                        <h4><span uk-icon="happy"></span> Get Your Encrypted File</h4>
                        <form>
                            <div class="uk-margin" uk-margin>
                                <button  id="modalEncrypt" class="uk-button uk-button-default">Download</button>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--begin contact -->
    <section class="section-dark" id="contact">
        <!--begin container-->
        <div class="container">
            <!--begin row-->
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center margin-bottom-40">
                    <h2 class="section-title grey">Get In Touch</h2>
                </div>
                <!--end col-md-10 -->
            </div>
            <!--end row-->
            <!--begin row-->
            <div class="row margin-bottom-30 wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">
                <!--begin success message -->
                <p class="contact_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                <!--end success message -->
                <!--begin contact form -->
                <form id="contact-form" class="contact" action="http://demo.epic-webdesign.com/tf-howdy/v1/php/contact.php" method="post">
                    <!--begin col-md-6-->
                    <div class="col-md-6">
                        <input class="contact-input white-input" required="" name="contact_names" placeholder="Full Name*" type="text">
                        <input class="contact-input white-input" required="" name="contact_email" placeholder="Email Adress*" type="email">
                        <input class="contact-input white-input" required="" name="contact_phone" placeholder="Phone Number*" type="text">
                    </div>
                    <!--end col-md-6-->
                    <!--begin col-md-6-->
                    <div class="col-md-6">
                        <textarea class="contact-commnent white-input" rows="2" cols="20" name="contact_message" placeholder="Your Message..."></textarea>
                    </div>
                    <!--end col-md-6-->
                    <!--begin col-md-12-->
                    <div class="col-md-12">
                        <input value="Send Message" id="submit-button" class="contact-submit" type="submit">
                    </div>
                    <!--end col-md-12-->
                </form>
                <!--end contact form -->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>