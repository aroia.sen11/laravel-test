<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    


    
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <meta name="description" content="">
    <meta name="author" content="Jeremy Kenedy">
    <title>LockaFile</title>
    <link rel="shortcut icon" href="/favicon.ico">


    
    
    <link href="<?php echo e(asset('assets/css/bootstrap.css')); ?>" rel="stylesheet">
    <!-- Loading Template CSS -->
    <link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">
    <!-- Loading UI KIT CSS -->
    <link href="<?php echo e(asset('assets/css/uikit.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/css/style-magnific-popup.css')); ?>" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin:500,600,700" rel="stylesheet">
    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="../../../cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/pe-icon-7-stroke.css')); ?>">
    <!-- Optional - Adds useful class to manipulate icon font display -->
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/helper.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/owl.theme.default.min.css')); ?>">

    <!-- Custom styles for this template-->
    <script src="<?php echo e(asset('assets/js/html5shiv.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/respond.min.js')); ?>"></script>



</head>
<body>

    <header class="header">
        <!--begin nav -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <!--begin container -->
            <div class="container">
                <!--begin navbar -->
                <div class="navbar-header">
                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--logo -->
                    <a href="#" class="navbar-brand" id="logo">LockaFile</a>
                </div>
                <div id="navbar-collapse-02" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo e(url('/home')); ?>">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li>
                            <a href="<?php echo e(url('/internal')); ?>">Get Started</a>


                        </li>
                        <li><a href="#pricing">Plans</a></li>
                        <li><a href="#contact">Contact Us</a></li>

                        <?php if(Auth::check()): ?>
                            <li><a href=""><?php echo e(Auth::user()->name); ?></a></li>
                        <li><a href="<?php echo e(route('logout')); ?>"  class="discover-btn">Logout</a></li>
                    <?php endif; ?>

                        <!--                        <li><a href="internal.html" class="discover-btn">Lock a File</a></li>-->
                    </ul>
                </div>
                <!--end navbar -->
            </div>
            <!--end container -->





        </nav>
        <!--end nav -->
    </header>
        <?php echo $__env->yieldContent('content'); ?>

    <div class="footer">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center">
                    <p>All Rights Researved &copy; LockaFile 2019</p>
                </div>
                <!--end col-md-6 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </div>

<!-- Load JS here for greater good =============================-->
<script src="<?php echo e(asset('assets/js/jquery-1.11.3.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/owl.carousel.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.scrollTo-min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.magnific-popup.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.nav.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/plugins.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/uou-accordions.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/uou-tabs.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/uikit.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/uikit-icons.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>


</body>
</html>
