<?php $__env->startSection('content'); ?>
    <section class="home-section" id="home_wrapper">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <div class="col-md-7 padding-top-40">
                    <h1>Secure your files, Start Now!</h1>
                    <p>Few steps to share encrypted files</p>
                    <ul class="home-benefits">
                        <li><i class="pe-7s-user hi-icon red"></i> Sign up to use LockAfile.</li>
                        <li><i class="pe-7s-mail hi-icon red"></i> Add the email addresses of all recipients.</li>
                        <li><i class="pe-7s-lock hi-icon red"></i> Lock (encrypt) your file.</li>
                        <li><i class="pe-7s-file hi-icon red"></i> Send the encrypted file.</li>
                    </ul>
                </div>
                <!--end col-md-7-->
                <!--begin col-md-5-->
                <div class="col-md-5">
                    <div class="login-sec">
                        <!-- TABS -->
                        <div class="uou-tabs">
                            <ul class="tabs">
                                <li class="active"><a href="#log-in">Login</a></li>
                                <li> <a href="#register">Register</a></li>
                            </ul>
                            <!-- REGISTER -->
                              <div class="content">
                                <div id="register">

                                    <form method="POST" action="<?php echo e(route('register')); ?>">
                                        <?php echo csrf_field(); ?>

                                        <div class="form-group row">
                                            <input id="name" type="text" placeholder="Name" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>
                                            <?php if($errors->has('name')): ?>
                                                <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                            <?php endif; ?>
                                        </div>


                                        <div class="form-group row">

                                                <input id="first_name" type="text" placeholder="First Name" class="form-control<?php echo e($errors->has('first_name') ? ' is-invalid' : ''); ?>" name="first_name" value="<?php echo e(old('first_name')); ?>" required autofocus>

                                                <?php if($errors->has('first_name')): ?>
                                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                        </div>





                                        <div class="form-group row">
                                                <input id="last_name" type="text" placeholder="Last Name" class="form-control<?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>" name="last_name" value="<?php echo e(old('last_name')); ?>" required autofocus>

                                                <?php if($errors->has('last_name')): ?>
                                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                        </div>

                                        <div class="form-group row">
                                                <input id="email" type="email" placeholder="Email Address" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

                                                <?php if($errors->has('email')): ?>
                                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                        </div>

                                        <div class="form-group row">
                                                <input id="password" type="password" placeholder="Password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                                                <?php if($errors->has('password')): ?>
                                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                        </div>

                                        <div class="form-group row">
                                                <input id="password-confirm" type="password" placeholder="Confirm Password" name="password_confirmation" required>

                                        </div>





                                        <label class="radio">
                                            <input type="radio" name="use" checked>Personal Use:
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="use">Business Use:
                                        </label><br>


                                        <div class="form-group row mb-4">
                                                <button type="submit" class="btn btn-primary">
                                                    <?php echo e(__('Register')); ?>

                                                </button>
                                            </div>




                                        <div class="login-with"> <span>Or Register With:</span> <a href="#."><span uk-icon="google"></span></a> <a href="#."><span uk-icon="facebook"></span></a> <a href="#."><span uk-icon="linkedin"></span></a> </div>
                                    </form>
                                </div>

                                <!-- LOGIN -->
                                <div id="log-in" class="active">
                                    <form method="POST" action="<?php echo e(route('login')); ?>">
                                        <?php echo csrf_field(); ?>
                                        <div class="form-group">
                                                <input id="email" type="email" placeholder="Email Address" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                                                <?php if($errors->has('email')): ?>
                                                    <span class="invalid-feedback">
                                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                        </div>

                                        <div class="form-group">
                                                <input id="password" type="password" placeholder="Password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>
                                                     <?php if($errors->has('password')): ?>
                                                         <span class="invalid-feedback">
                                                         <strong><?php echo e($errors->first('password')); ?></strong>
                                                         </span>
                                                <?php endif; ?>
                                        </div>
                                        <button type="submit">Login</button>
                                        <div class="login-with"> <span>Or Login With:</span> <a href="#."><span uk-icon="google"></span></a> <a href="#."><span uk-icon="facebook"></span></a> <a href="#."><span uk-icon="linkedin"></span></a> </div>
                                        <div class="forget">Forgot your password? <a href="#.">Click Here</a></div>
                                    </form>
                                </div>
                                <div id="forget">
                                    <form>
                                        <input type="email" placeholder="Email Address">
                                        <button type="submit">Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end home section -->
    <!--begin section-white -->
    <section class="section-white" id="about">
        <div class="container">
            <div class="col-md-12 text-center padding-bottom-20">
                <h4 class="section-title">About</h4>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <img src="<?php echo e(asset('assets/images/about.png')); ?>" class="width-100 picture-padding-bottom" alt="pic">
                </div>
                <div class="col-md-8 padding-top-40">
                    <p>LockAfile is a simple way to secure your sensitive files at anywhere without worrying about moving them or remembering any password.</p>
                    <ul class="features-list-hero">
                        <li class="wow fadeIn" data-wow-delay="0.25s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">
                            <i class="pe-7s-star hi-icon blue"></i>
                            100% GDPR compliant. No requirements to install a software No need to move your files from where they currently are Lockafile will not have access to your file.
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 1s; animation-name: fadeIn;">
                            <i class="pe-7s-lock hi-icon lyla"></i>
                            Unlike our competitors, we don’t move your files. Instead, we provide you with the ability to secure (encrypt) your files (any type).
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.75s" style="visibility: visible; animation-delay: 1.25s; animation-name: fadeIn;">
                            <i class="pe-7s-file hi-icon blue"></i>
                            Use LockaFile exactly where you currently store them (e.g. your local computer, mobile phone, tablets, email, dropbox, …etc).
                        </li>
                        <li class="wow fadeIn" data-wow-delay="0.75s" style="visibility: visible; animation-delay: 1.25s; animation-name: fadeIn;">
                            <i class="pe-7s-settings hi-icon lyla"></i>
                            You will always have a full control of your file and at any time you can stop people from accessing your files.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--begin pricing section -->
    <section class="section-grey padding-bottom-40 padding-top-40" id="pricing">
        <!--begin container -->
        <div class="container">
            <!--begin row -->
            <div class="row">
                <!--begin col-md-12 -->
                <div class="col-md-12 text-center padding-bottom-40">
                    <h4 class="section-title">Choose the pricing that fits your needs</h4>
                </div>
                <!--end col-md-12 -->
                <!--begin col-md-4-->
                <div class="col-md-6 col-sm-6">
                    <div class="price-box-white">
                        <ul class="pricing-list">
                            <li class="price-title">Personal Use</li>
                            <li class="price-value">Free</li>
                            <li class="price-tag"><a href="#">GET STARTED</a></li>
                        </ul>
                    </div>
                </div>
                <!--end col-md-6 -->
                <!--begin col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <div class="price-box-blue">
                        <ul class="pricing-list">
                            <li class="price-title white-text">Business Use</li>
                            <li class="price-value white-text">$19 Per Month</li>
                            <li class="price-tag"><a href="#">GET STARTED</a></li>
                        </ul>
                    </div>
                </div>
                <!--end col-md-4 -->
            </div>
            <!--end row -->
        </div>
        <!--end container -->
    </section>
    <!--end pricing section -->
    <!--begin contact -->
    <section class="section-dark" id="contact">
        <!--begin container-->
        <div class="container">
            <!--begin row-->
            <div class="row">
                <!--begin col-md-10 -->
                <div class="col-md-10 col-md-offset-1 text-center margin-bottom-40">
                    <h2 class="section-title grey">Get In Touch</h2>
                </div>
                <!--end col-md-10 -->
            </div>
            <!--end row-->
            <!--begin row-->
            <div class="row margin-bottom-30 wow bounceIn" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;">
                <!--begin success message -->
                <p class="contact_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                <!--end success message -->
                <!--begin contact form -->
                <form id="contact-form" class="contact" action="http://demo.epic-webdesign.com/tf-howdy/v1/php/contact.php" method="post">
                    <!--begin col-md-6-->
                    <div class="col-md-6">
                        <input class="contact-input white-input" required="" name="contact_names" placeholder="Full Name*" type="text">
                        <input class="contact-input white-input" required="" name="contact_email" placeholder="Email Adress*" type="email">
                        <input class="contact-input white-input" required="" name="contact_phone" placeholder="Phone Number*" type="text">
                    </div>
                    <!--end col-md-6-->
                    <!--begin col-md-6-->
                    <div class="col-md-6">
                        <textarea class="contact-commnent white-input" rows="2" cols="20" name="contact_message" placeholder="Your Message..."></textarea>
                    </div>
                    <!--end col-md-6-->
                    <!--begin col-md-12-->
                    <div class="col-md-12">
                        <input value="Send Message" id="submit-button" class="contact-submit" type="submit">
                    </div>
                    <!--end col-md-12-->
                </form>
                <!--end contact form -->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app-js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>