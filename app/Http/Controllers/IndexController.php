<?php

namespace App\Http\Controllers;
use Auth;
class IndexController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
            return view('home');
        else
            return view('index');
    }
}
