var sendMethod = 'gmail';
var recorder;
var expiryValue = '0';
var lastwriter = null;

function setupEditor(_0xef97x6) {
    _0xef97x6['addButton']('send', {
        tooltip: 'Send Secure Encrypted Message',
        icon: true,
        image: '/gmail/gmail32.png',
        onclick: function() {
            sendEmail()
        }
    });
    if (sendMethod == 'gmail') {
        _0xef97x6['addButton']('gdrive', {
            tooltip: 'Attach Files From Google Drive',
            icon: true,
            image: '/gmail/gd32.png',
            onclick: function() {
                selectFromDrive()
            }
        })
    } else {
        if (sendMethod == 'o365') {
            _0xef97x6['addButton']('gdrive', {
                tooltip: 'Attach Files From Google Drive',
                icon: true,
                image: '/o365/od32.png',
                onclick: function() {
                    selectFromDrive()
                }
            })
        }
    };
    _0xef97x6['addButton']('fattach', {
        tooltip: 'Attach Files From Local Device',
        icon: true,
        image: '/gmail/attach.gif',
        onclick: function() {
            selectFromLocal()
        }
    });
    _0xef97x6['addButton']('expiry', {
        type: 'listbox',
        tooltip: 'Set Message Expiry Time',
        icon: true,
        image: 'expiry.gif',
        onselect: function(_0xef97x7) {
            expiryValue = this['value']()
        },
        values: [{
            text: '10 Minutes',
            value: '600'
        }, {
            text: '30 Minutes',
            value: '1800'
        }, {
            text: '1 Hour',
            value: '3600'
        }, {
            text: '3 Hours',
            value: '10800'
        }, {
            text: '6 Hours',
            value: '21600'
        }, {
            text: '12 Hours',
            value: '43200'
        }, {
            text: '1 Day',
            value: '86400'
        }, {
            text: '3 Days',
            value: '259200'
        }, {
            text: '5 Days',
            value: '432000'
        }, {
            text: '1 Week',
            value: '604800'
        }, {
            text: '10 Days',
            value: '864000'
        }, {
            text: '2 Weeks',
            value: '1209600'
        }, {
            text: '1 Month',
            value: '2592000'
        }, {
            text: '3 Months',
            value: '7776000'
        }, {
            text: '6 Months',
            value: '15552000'
        }, {
            text: '1 Year',
            value: '31104000'
        }, {
            text: 'Never',
            value: '0'
        }],
        onPostRender: function() {
            this['value'](expiryValue)
        }
    });
    if (navigator['mediaDevices'] && navigator['mediaDevices']['getUserMedia']) {
        recorder = new voiceRecorder();
        _0xef97x6['addButton']('speech', {
            tooltip: 'Speak to record speech',
            icon: true,
            image: 'https://www.lockmagic.com/voice/mic.png',
            onclick: function() {
                var _0xef97x8 = document['getElementById'](this._id);
                var _0xef97x9 = '/mic.png';
                var _0xef97xa = '/micon.gif';
                if (_0xef97x8['outerHTML']['indexOf'](_0xef97x9) > 0) {
                    recorder['start']()
                } else {
                    _0xef97x9 = '/micon.gif';
                    _0xef97xa = '/micbusy.gif';
                    recorder['stop'](function(_0xef97xb, _0xef97xc) {
                        var _0xef97x8 = document['getElementById'](_0xef97xb['div']);
                        if (_0xef97x8) {
                            _0xef97x8['outerHTML'] = _0xef97x8['outerHTML']['replace']('/micbusy.gif', '/mic.png')
                        };
                        var _0xef97xd = new FileReader();
                        _0xef97xd['onload'] = function(_0xef97x7) {
                            var _0xef97xe = voices['length'];
                            var _0xef97xf = '<audio id=\'voiceid' + _0xef97xe + '\' src=\'' + this['result'] + '\'></audio>';
                            voices[_0xef97xe] = this['result'];
                            tinyMCE['activeEditor']['execCommand']('mceInsertRawHTML', false, _0xef97xf)
                        };
                        _0xef97xd['readAsDataURL'](_0xef97xc)
                    }, {
                        div: this['_id']
                    })
                };
                _0xef97x8['outerHTML'] = _0xef97x8['outerHTML']['replace'](_0xef97x9, _0xef97xa)
            }
        })
    }
}

function initTinyEditor(_0xef97x11) {
    var _0xef97x12 = {
        selector: '#value',
        inline: true,
        menubar: false,
        paste_data_images: true,
        theme: 'modern',
        toolbar_items_size: 'medium',
        statusbar: false,
        init_instance_callback: function(_0xef97x6) {
            _0xef97x6['fire']('focus')
        },
        plugins: ['advlist autolink lists link image charmap anchor preview', 'searchreplace visualblocks fullscreen spellchecker directionality emoticons', 'insertdatetime media table textcolor colorpicker textpattern paste imagetools hr '],
        toolbar: 'expiry | bold italic underline |  bullist numlist | outdent indent | forecolor backcolor | fontselect fontsizeselect | insert emoticons removeformat'
    };
    var _0xef97x13 = 'send | gdrive fattach ';
    if (is_mobile() && window['innerWidth'] < 800) {
        _0xef97x12['toolbar'] = 'expiry | bold italic | bullist numlist | forecolor backcolor emoticons';
        _0xef97x13 = 'gdrive fattach '
    } else {
        _0xef97x12['fixed_toolbar_container'] = '#toolbar'
    };
    if (navigator['mediaDevices'] && navigator['mediaDevices']['getUserMedia']) {
        _0xef97x12['toolbar'] = 'speech ' + _0xef97x12['toolbar']
    };
    _0xef97x12['toolbar'] = _0xef97x13 + _0xef97x12['toolbar'];
    _0xef97x12['setup'] = setupEditor;
    tinymce['init'](_0xef97x12);
    document['getElementById']('value')['focus']()
}
var instructionMsg = ' sent secure, encrypted message.<br><br><br><b>Download attachment and open using a web browser.</b>';
var replay = null;

function WrapHtmlEmailMessage(_0xef97xb, _0xef97x17, _0xef97x18, _0xef97x19) {
    if (typeof _0xef97x19 == 'undefined') {
        _0xef97x19 = null
    };
    var _file_name;
    if (_0xef97xb['attachname']) {
        _file_name = _0xef97xb['attachname']
    } else {
        if (_0xef97xb['metadata']['subject']) {
            _file_name = _0xef97xb['metadata']['subject']
        } else {
            _file_name = _0xef97xb['metadata']['from']
        }
    };
    var _0xef97x1b = base64EncArr(_0xef97x17['filebuffer'], _0xef97x17['filebuffer']['length']);
    var _file_data;
    var _extension;
    if (_0xef97xb['storename'] == 'html') {
        var _0xef97x1e = window['atob'](g_htmlappkeytemplatemsg);
        _0xef97x1e = _0xef97x1e['replace']('%MessageTitle%', _0xef97xb['metadata']['subject']);
        _0xef97x1e = _0xef97x1e['replace']('%From%', _0xef97xb['metadata']['from']);
        _0xef97x1e = _0xef97x1e['replace']('%Filename%', _0xef97xb['filename']);
        _0xef97x1e = _0xef97x1e['replace']('%Blob%', _0xef97x1b);
        replay = new Object();
        replay['body'] = _0xef97x1e;
        replay['metadata'] = _0xef97xb['metadata'];
        replay['name'] = _file_name + '_wef.html';
        replay['url'] = 'https://www.lockmagic.com/efssvc/femail.ashx';
        replay['geturl'] = _0xef97xb['geturl'];
        _0xef97x1e = window['btoa'](_0xef97x1e);
        _file_data = _0xef97x1e;
        _extension = '_wef.html'
    } else {
        if (_0xef97xb['storename'] == 'wef') {
            _file_data = _0xef97x1b;
            _extension = '.wef'
        } else {
            return true
        }
    };
    var _0xef97x1f = new Object();
    _0xef97x1f['filename'] = _file_name + _extension;
    _0xef97x1f['filedata'] = _file_data;
    _0xef97x1f['mime'] = 'application/octet-stream';
    var _0xef97x20 = new Array();
    _0xef97x20[0] = _0xef97x1f;
    var _0xef97x21 = new Object();
    _0xef97x21['from'] = _0xef97xb['metadata']['from'];
    _0xef97x21['to'] = _0xef97xb['metadata']['to'];
    _0xef97x21['cc'] = _0xef97xb['metadata']['cc'];
    _0xef97x21['bcc'] = _0xef97xb['metadata']['bcc'];
    _0xef97x21['subject'] = _0xef97xb['metadata']['subject'];
    var _0xef97x22 = window['atob'](_0xef97x19 ? g_templatemsgcard : g_templatemsg);
    _0xef97x22 = _0xef97x22['replace']('%MSGLINK%', _0xef97x19 ? _0xef97x19 : '');
    _0xef97x22 = _0xef97x22['replace']('%MSGLINK%', _0xef97x19 ? _0xef97x19 : '');
    _0xef97x21['body'] = _0xef97x22['replace']('%MessageTitle%', _0xef97xb['metadata']['from'] + instructionMsg);
    var _0xef97x23 = buildemlwrap(_0xef97x21['from'], _0xef97x21['to'], _0xef97x21['cc'], _0xef97x21['bcc'], _0xef97x21['subject'], null, _0xef97x21['body'], _0xef97x20);
    _0xef97xb['callback'](_0xef97xb, _0xef97x23);
    return false
}

function smtpSend(_0xef97x25, _0xef97xb, _0xef97x26, _0xef97x27, _0xef97x28) {
    if (g_statusText) {
        g_statusText['innerHTML'] = 'Sending email message via Lockmagic...'
    };
    var _0xef97x29 = getXmlHttpHandle();
    _0xef97x29['onreadystatechange'] = function(_0xef97x7) {
        if (_0xef97x29['readyState'] == 4) {
            if (g_statusText) {
                g_statusText['innerHTML'] = '<br>Email sent....'
            };
            doneProcessing();
            sendDone(null)
        }
    };
    _0xef97x29['onerror'] = function(_0xef97x7) {
        setProgress(0, 'XHR error.');
        if (g_statusText) {
            g_statusText['innerHTML'] = '<br>Email error....'
        };
        doneProcessing()
    };
    _0xef97x29['upload']['onprogress'] = function(_0xef97x7) {
        if (_0xef97x7['lengthComputable']) {
            var _0xef97x2a = Math['round']((_0xef97x7['loaded'] / _0xef97x7['total']) * 100);
            setProgress(_0xef97x2a, _0xef97x2a == 0 ? 'Processing data...' : 'Email in progress...')
        }
    };
    _0xef97x29['open']('POST', _0xef97x25, true);
    _0xef97x29['setRequestHeader']('X-Mail-Filename', _0xef97x26);
    _0xef97x29['setRequestHeader']('X-Mail-UserList', striprole(_0xef97xb['to']));
    if (_0xef97xb['subject'] != null && _0xef97xb['subject']['length'] > 0) {
        _0xef97x29['setRequestHeader']('X-Mail-Subject', _0xef97xb['subject'])
    };
    if (_0xef97xb['to'] != null && _0xef97xb['to']['length'] > 0) {
        _0xef97x29['setRequestHeader']('X-Mail-ToList', striprole(_0xef97xb['to']))
    };
    if (_0xef97xb['cc'] != null && _0xef97xb['cc']['length'] > 0) {
        _0xef97x29['setRequestHeader']('X-Mail-CcList', striprole(_0xef97xb['cc']))
    };
    if (_0xef97xb['bcc'] != null && _0xef97xb['bcc']['length'] > 0) {
        _0xef97x29['setRequestHeader']('X-Mail-BccList', striprole(_0xef97xb['bcc']))
    };
    if (_0xef97x28 != null && _0xef97x28['length'] > 0) {
        _0xef97x29['setRequestHeader']('X-Mail-GetLink', _0xef97x28)
    };
    _0xef97x29['setRequestHeader']('content-type', 'application/octet-stream');
    g_xhttp = _0xef97x29;
    _0xef97x29['send'](_0xef97x27);
    return false
}

function sendDone(_0xef97x2c) {
    if (_0xef97x2c && _0xef97x2c['error']) {
        if (_0xef97x2c['error']['code'] > 299) {
            if (_0xef97x2c['error']['data']['length'] > 0 && _0xef97x2c['error']['data'][0]['reason'] == 'failedPrecondition') {
                if (replay) {
                    smtpSend(replay['url'], replay['metadata'], replay['name'], replay['body'], replay['geturl']);
                    return
                }
            }
        };
        if (g_statusText) {
            g_statusText['innerHTML'] = 'Failed to send ' + _0xef97x2c['error']['code'] + ' ' + _0xef97x2c['error']['message']
        };
        return
    };
    ModalPopupsCancel();
    self['close']();
    window['close']();
    document['body']['innerHTML'] = '<center><br><br><br><h1 style=\'font-size:30px;\'>Message successful sent!</h1><br><br><p style=\'font-size:20px;weight=bold;\'>Please manually close this window</p></center>'
}

function gmailSend(_0xef97xb, _0xef97x2e) {
    if (g_statusText) {
        g_statusText['innerHTML'] = 'Sending gmail message...'
    };
    if (sendMethod == 'native') {
        smtpSend(replay['url'], replay['metadata'], replay['name'], replay['body'], replay['geturl']);
        return
    };
    var _0xef97x2f = {
        "\x75\x73\x65\x72\x49\x64": 'me',
        "\x72\x65\x73\x6F\x75\x72\x63\x65": {
            "\x72\x61\x77": window['btoa'](_0xef97x2e)['replace'](/\+/g, '-')['replace'](/\//g, '_')
        }
    };
    if (_0xef97x2e['length'] > (1024 * 1024)) {
        doGmailSend(_0xef97x2e)
    } else {
        var _0xef97x30 = gapi['client']['gmail']['users']['messages']['send'](_0xef97x2f);
        return _0xef97x30['execute'](sendDone)
    }
}

function doGmailSend(_0xef97x2f) {
    var _0xef97x29 = getXmlHttpHandle();
    var _0xef97x25 = 'https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media';
    _0xef97x29['open']('POST', _0xef97x25, true);
    _0xef97x29['onreadystatechange'] = function(_0xef97x7) {
        if (_0xef97x29['readyState'] == 4) {
            var _0xef97x32 = {
                status: _0xef97x29['status'],
                value: _0xef97x29['responseText']
            };
            if (_0xef97x32['status'] == 200) {
                sendDone(null)
            } else {
                if (replay) {
                    smtpSend(replay['url'], replay['metadata'], replay['name'], replay['body'], replay['geturl']);
                    return
                }
            }
        }
    };
    _0xef97x29['onerror'] = function(_0xef97x7) {
        if (g_statusText) {
            g_statusText['innerHTML'] = '<br>Email error....'
        }
    };
    _0xef97x29['setRequestHeader']('Content-Type', 'message/rfc822');
    _0xef97x29['setRequestHeader']('Content-Length', _0xef97x2f['length']);
    var _0xef97x33 = gapi['auth']['getToken']()['access_token'];
    if (_0xef97x33) {
        _0xef97x29['setRequestHeader']('Authorization', 'Bearer ' + _0xef97x33)
    };
    _0xef97x29['send'](_0xef97x2f)
}
var clientId = '944827222186-uvppahb5lrciv7tkppu147aomrd1aq1o.apps.googleusercontent.com';
var scopes = 'https://www.googleapis.com/auth/gmail.send';
var attachscopes = 'https://www.googleapis.com/auth/drive.readonly';

function handleClientLoad() {
    window['setTimeout'](checkAuth, 1)
}

function checkAuth() {
    gapi['auth']['authorize']({
        client_id: clientId,
        scope: scopes,
        immediate: true
    }, handleAuthResult)
}

function checkAuthAttach() {
    gapi['auth']['authorize']({
        client_id: clientId,
        scope: attachscopes,
        immediate: false
    }, handleAttachAuthResult)
}

function handleAuthClick() {
    gapi['auth']['authorize']({
        client_id: clientId,
        scope: scopes,
        immediate: false
    }, handleAuthResult);
    return false
}

function handleAuthResult(_0xef97x3c) {
    var _0xef97x3d = document['getElementById']('authorize-div');
    if (_0xef97x3c && !_0xef97x3c['error']) {
        _0xef97x3d['style']['display'] = 'none';
        loadGmailApi();
        document['getElementById']('main')['style']['display'] = 'block'
    } else {
        _0xef97x3d['style']['display'] = 'inline';
        document['getElementById']('main')['style']['display'] = 'none'
    }
}

function handleAttachAuthResult(_0xef97x3c) {
    var _0xef97x3d = document['getElementById']('attachauthorize-div');
    if (_0xef97x3c && !_0xef97x3c['error']) {
        _0xef97x3d['style']['display'] = 'none';
        loadAttachDriveApi()
    } else {
        _0xef97x3d['style']['display'] = 'inline'
    }
}

function loadGmailApi() {
    gapi['client']['load']('gmail', 'v1', startApp)
}

function loadAttachDriveApi() {
    gapi['client']['load']('drive', 'v3', startPicker)
}
var gdattachments = null;

function attachmentSelected(_0xef97x43) {
    id = document['getElementById']('gdattach');
    if (_0xef97x43['length'] > 1) {
        id['textContent'] = _0xef97x43['length'] + ' items selected'
    } else {
        id['textContent'] = _0xef97x43[0]['name']
    };
    gdattachments = _0xef97x43
}

function startPicker() {
    var _0xef97x45 = new GdFilePicker({
        callback: attachmentSelected
    });
    setTimeout(function() {
        window['focus']()
    }, 1000)
}

function selectFromDrive() {
    checkAuthAttach()
}

function gdDownloadItem(_0xef97x48, _file_name, _0xef97x49, _0xef97x33, _0xef97x4a, _0xef97x4b) {
    _pgdDownloadItem(0, _0xef97x48, _file_name, _0xef97x49, _0xef97x33, _0xef97x4a, _0xef97x4b)
}

function _pgdDownloadItem(_0xef97x4d, _0xef97x48, _file_name, _0xef97x49, _0xef97x33, _0xef97x4a, _0xef97x4b) {
    var _0xef97x4e = createCORSRequest('GET', _0xef97x49);
    _0xef97x4e['onload'] = function() {
        if (_0xef97x4e['readyState'] == _0xef97x4e['DONE']) {
            if (_0xef97x4e['response'] && (_0xef97x4e['status'] == 0 || _0xef97x4e['status'] == 200)) {
                var _0xef97x18 = _0xef97x4e['response'];
                var _0xef97x4f = _0xef97x4e['getResponseHeader']('content-type');
                _0xef97x4a(_0xef97x4b, _0xef97x18, _0xef97x4f)
            } else {
                _0xef97x4a(_0xef97x4b, null)
            }
        }
    };
    _0xef97x4e['onerror'] = function() {
        if ((typeof _0xef97x4e['status'] == 'undefined' || _0xef97x4e['status'] == 502 || _0xef97x4e['status'] == 0) && _0xef97x4d < 3) {
            _pgdDownloadItem(_0xef97x4d + 1, _0xef97x48, _file_name, _0xef97x49, _0xef97x33, _0xef97x4a, _0xef97x4b);
            return
        };
        setProgress(0, 'XHR error.');
        g_statusText['innerHTML'] = '<br>Downloading...' + _file_name + ' failed: ' + _0xef97x4e['status'];
        doneProcessing()
    };
    _0xef97x4e['onprogress'] = function(_0xef97x7) {
        if (_0xef97x7['lengthComputable']) {
            var _0xef97x2a = Math['round']((_0xef97x7['loaded'] / _0xef97x7['total']) * 100);
            setProgress(_0xef97x2a, _0xef97x2a == 100 ? 'Finalizing.' : 'Uploading.')
        }
    };
    _0xef97x4e['onreadystatechange'] = function(_0xef97x7) {
        if (_0xef97x4e['readyState'] == 1) {
            g_statusText['innerHTML'] += '<br>Connecting...'
        };
        if (_0xef97x4e['readyState'] == 2) {
            g_statusText['innerHTML'] += '<br>Downloading...' + _file_name
        };
        if (_0xef97x4e['readyState'] == 4) {
            g_statusText['innerHTML'] += '<br>Download done....'
        }
    };
    _0xef97x4e = openCORSRequest(_0xef97x4e, 'GET', _0xef97x49);
    if (_0xef97x33) {
        _0xef97x4e['setRequestHeader']('Authorization', 'Bearer ' + _0xef97x33)
    };
    _0xef97x4e['responseType'] = 'arraybuffer';
    _0xef97x4e['send']()
}

function LmContacts(_0xef97x51) {
    if (_0xef97x51) {;
    } else {
        if (sendMethod == 'o365') {
            _0xef97x51 = '/o365/lmcontacts.html'
        } else {
            _0xef97x51 = '/apps/lmcontacts.html'
        }
    };
    window['open'](_0xef97x51, '_blank', 'location=0, width=650, height=420, left=150, top=150, menubar=0,  resizable=1, scrollbars=0, status=0, titlebar=0,')
}

function ModalPopupsCancel() {
    doneProcessing();
    if (g_xhttp) {
        g_xhttp['abort']()
    };
    ModalPopups.Cancel('idCustom1')
}

function encode4HTML(_0xef97x54) {
    return _0xef97x54['replace'](/\r\n?/g, '\x0A')['replace'](/(^((?!\n)\s)+|((?!\n)\s)+$)/gm, '')['replace'](/(?!\n)\s+/g, ' ')['replace'](/^\n+|\n+$/g, '')['replace'](/[<>&"']/g, function(_0xef97x1f) {
        switch (_0xef97x1f) {
            case '<':
                return '&lt;';
            case '>':
                return '&gt;';
            case '&':
                return '&amp;';
            case '"':
                return '&quot;';
            case '\'':
                return '&apos;'
        }
    })['replace'](/\n{2,}/g, '</p><p>')['replace'](/\n/g, '<br />')['replace'](/^(.+?)$/, '<p>$1</p>')
}

function attachDownloadComplete(_0xef97xb, _0xef97x18, _0xef97x4f) {
    if (_0xef97x18 != null) {
        var _0xef97x48 = _0xef97xb['items'][_0xef97xb['index']];
        var _0xef97x56 = new Object();
        _0xef97x56['filename'] = _0xef97x48['name'];
        if (_0xef97xb['options']['isviewer'] && _0xef97x48['pdfname']) {
            _0xef97x56['filename'] = _0xef97x48['pdfname']
        };
        _0xef97x56['filedata'] = null;
        _0xef97x56['arraybuffer'] = _0xef97x18;
        _0xef97x56['mime'] = _0xef97x4f ? _0xef97x4f : GetFileMimeAsIs(_0xef97x56['filename']);
        _0xef97xb['arr'][_0xef97xb['arr']['length']] = _0xef97x56
    };
    _0xef97xb['index']++;
    if (_0xef97xb['index'] == _0xef97xb['items']['length']) {
        _0xef97xb['callback'](_0xef97xb);
        return
    };
    var _0xef97x48 = _0xef97xb['items'][_0xef97xb['index']];
    var _file_name = _0xef97x48['name'];
    var _0xef97x49 = _0xef97x48['directurl'];
    if (_0xef97xb['options']['isviewer'] && _0xef97x48['pdfname']) {
        _file_name = _0xef97x48['pdfname'];
        _0xef97x49 = _0xef97x48['pdfurl']
    };
    g_statusText['innerHTML'] = '<b>Downloading file ' + _file_name + '...</b>';
    gdDownloadItem(_0xef97x48, _file_name, _0xef97x49, _0xef97x48['authtoken'], attachDownloadComplete, _0xef97xb)
}

function CreateEmlContinue(_0xef97xb) {
    pCreateEml(_0xef97xb['textval'], _0xef97xb['acl'], _0xef97xb['ccacl'], _0xef97xb['bccacl'], _0xef97xb['arr'], _0xef97xb['options'])
}

function CreateEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x20, _0xef97x5d) {
    if (gdattachments != null && gdattachments['length'] > 0) {
        var _0xef97x5e = new Object();
        if (_0xef97x20 == null) {
            _0xef97x20 = new Array()
        };
        _0xef97x5e['textval'] = _0xef97x59;
        _0xef97x5e['acl'] = _0xef97x5a;
        _0xef97x5e['ccacl'] = _0xef97x5b;
        _0xef97x5e['bccacl'] = _0xef97x5c;
        _0xef97x5e['arr'] = _0xef97x20;
        _0xef97x5e['options'] = _0xef97x5d;
        _0xef97x5e['callback'] = CreateEmlContinue;
        _0xef97x5e['index'] = -1;
        _0xef97x5e['items'] = gdattachments;
        attachDownloadComplete(_0xef97x5e, null);
        return
    };
    pCreateEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x20, _0xef97x5d)
}

function pCreateEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x20, _0xef97x5d) {
    if (_0xef97x20 == null && _0xef97x59['trim']()['length'] == 0) {
        g_statusText['innerHTML'] = '<b>Empty email!</b><br>Enter message or attachments and try again.';
        return
    };
    var _0xef97x60 = '<html><body>' + _0xef97x59 + '</body></html>';
    var _0xef97x61 = document['getElementById']('subject')['value'];
    if (_0xef97x5a == null) {
        _0xef97x5a = ''
    };
    if (_0xef97x5b == null) {
        _0xef97x5b = ''
    };
    if (_0xef97x5c == null) {
        _0xef97x5c = ''
    };
    var _0xef97x62 = _0xef97x5a['replace'](/:v/gi, '');
    _0xef97x62 = _0xef97x62['replace'](/:e/gi, '');
    _0xef97x62 = _0xef97x62['replace'](/:o/gi, '');
    var _0xef97x63 = _0xef97x5b['replace'](/:v/gi, '');
    _0xef97x63 = _0xef97x63['replace'](/:e/gi, '');
    _0xef97x63 = _0xef97x63['replace'](/:o/gi, '');
    if (_0xef97x62['length'] > 0 && _0xef97x63['length'] > 0) {
        _0xef97x62 += ';' + _0xef97x63
    } else {
        if (_0xef97x62['length'] == 0) {
            _0xef97x62 = _0xef97x63
        }
    };
    var _0xef97x64 = _0xef97x5c['replace'](/:v/gi, '');
    _0xef97x64 = _0xef97x64['replace'](/:e/gi, '');
    _0xef97x64 = _0xef97x64['replace'](/:o/gi, '');
    var _0xef97x65 = getFromEmail(lockmagic_currentuser, '@gmail.com');
    if (_0xef97x65 == null) {
        alert('Please login to lockmagic service');
        return
    };
    var _0xef97x66 = _0xef97x65;
    if (_0xef97x5a['length'] > 0) {
        _0xef97x66 += ';' + _0xef97x5a
    };
    if (_0xef97x5b['length'] > 0) {
        _0xef97x66 += ';' + _0xef97x5b
    };
    if (_0xef97x5c['length'] > 0) {
        _0xef97x66 += ';' + _0xef97x5c
    };
    _0xef97x5a = _0xef97x66;
    var _0xef97x23 = buildeml(_0xef97x65, _0xef97x62['replace'](/;/g, ','), _0xef97x61, _0xef97x59, _0xef97x60, _0xef97x20);
    var _0xef97x67 = utf82ab(_0xef97x23);
    var _0xef97x68 = 'Message.eml';
    if (_0xef97x61 != null && _0xef97x61['length'] > 0) {
        _0xef97x68 = _0xef97x61 + '.eml'
    };
    var _0xef97x21 = new Object();
    _0xef97x21['from'] = _0xef97x65;
    _0xef97x21['to'] = _0xef97x62['replace'](/;/g, ',');
    _0xef97x21['cc'] = _0xef97x63['replace'](/;/g, ',');
    _0xef97x21['bcc'] = _0xef97x64['replace'](/;/g, ',');;;
    _0xef97x21['subject'] = _0xef97x61;
    pDoEncryptBufferArray(_0xef97x5a, _0xef97x68, _0xef97x61, _0xef97x67, _0xef97x5d['isviewer'], _0xef97x5d['isemail'], _0xef97x5d['iss3'], _0xef97x5d['cloudstore'], _0xef97x21)
}

function LoadFileForEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x6a, _0xef97x20, _0xef97xe, _0xef97x6b, _0xef97x5d) {
    var _0xef97x6c = new FileReader();
    _0xef97x6c['onloadend'] = function(_0xef97x7) {
        var _0xef97x18 = _0xef97x7['target']['result'];
        var _0xef97x56 = new Object();
        _0xef97x56['filename'] = _0xef97x6a['name'];
        _0xef97x56['filedata'] = _0xef97x18;
        _0xef97x20[_0xef97xe] = _0xef97x56;
        if (_0xef97xe + 1 == _0xef97x6b) {
            CreateEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x20, _0xef97x5d)
        }
    };
    _0xef97x6c['readAsDataURL'](_0xef97x6a)
}

function InitProgress(_0xef97x6e) {
    var _0xef97x1c = document['getElementById']('statusdiv.TMP.')['innerHTML'];
    _0xef97x1c = _0xef97x1c['replace'](/\.TMP\./g, '');
    ModalPopups.Custom('idCustom1', _0xef97x6e, _0xef97x1c, {
        width: (window['innerWidth'] < 480 ? 220 : 520),
        height: 230
    });
    g_statusText = document['getElementById']('statusmsg')
}

function shortUrlCallback(_0xef97xb, _0xef97x70) {
    var _0xef97x71;
    if (_0xef97xb['storename'] == 'gmail') {
        var _0xef97x22 = window['atob'](g_htmltemplatelinkmsg);
        _0xef97x22 = _0xef97x22['replace']('%MessageTitle%', _0xef97xb['metadata']['from'] + ' shared secure content with you.');
        _0xef97x22 = _0xef97x22['replace'](/%LINKVAL%/g, _0xef97x70);
        _0xef97x22 = _0xef97x22['replace']('%VERB%', 'Open');
        _0xef97x22 = _0xef97x22['replace']('%MAINVERB%', 'Open Message');
        _0xef97x22 = _0xef97x22['replace']('%COMMENT%', 'message content without installing software!');
        var _0xef97x23 = buildemlwrap(_0xef97xb['metadata']['from'], _0xef97xb['metadata']['to'], _0xef97xb['metadata']['cc'], _0xef97xb['metadata']['bcc'], _0xef97xb['subject'], null, _0xef97x22, null);
        gmailSend(_0xef97xb, _0xef97x23);
        return
    };
    if (_0xef97xb['storename'] == 'hotmail') {
        _0xef97x71 = BuildHotmailLink(_0xef97xb['metadata']['to'], _0xef97xb['metadata']['cc'], _0xef97xb['metadata']['bcc'], _0xef97xb['subject'], _0xef97xb['filename'], _0xef97x70)
    } else {
        alert('Unsupport email method: ' + _0xef97xb['storename']);
        return
    };
    window['location'] = _0xef97x71
}

function sendLinkHandler(_0xef97xb, _0xef97x17, _0xef97x18) {
    var _0xef97x73 = _0xef97x18['size'];
    if (_0xef97x73 < 0x72) {
        _0xef97x73 = _0xef97x17['total']
    };
    var _0xef97x74 = createCORSRequest('PUT', _0xef97xb['puturl']);
    if (!_0xef97x74) {
        setProgress(0, 'CORS not supported');
        return
    };
    showDiv('progress_bar');
    _0xef97x74['onloadend'] = function() {
        if (_0xef97x74['readyState'] == 4 && _0xef97x74['status'] == 200) {
            var _0xef97x19 = 'https://www.lockmagic.com/apps/lmdec.html?l=' + _0xef97xb['geturl'];
            if (_0xef97xb['guestallowed'] == true) {
                _0xef97x19 += '&g=1'
            };
            if (_0xef97xb['storename'] == 'gmail' || _0xef97xb['storename'] == 'hotmail') {
                _0xef97xb['storename'] = 'html';
                _0xef97xb['attachname'] = _0xef97xb['filename'];
                _0xef97xb['callback'] = gmailSend;
                instructionMsg += '<center>';
                instructionMsg += '<br><b>Mobile Users: <a href=\'' + _0xef97x19 + '\'>Open Message</a></b><br>';
                instructionMsg += '</center>';
                WrapHtmlEmailMessage(_0xef97xb, _0xef97x17, _0xef97x18, _0xef97x19);
                setProgress(0, 'Sending ' + _0xef97xb['storename'] + '....')
            } else {
                S3EmailHandler(_0xef97xb, _0xef97x17, _0xef97x18)
            }
        } else {
            if (_0xef97x74['readyState'] == 4) {
                setProgress(0, 'Upload error: ' + _0xef97x74['status'])
            }
        }
    };
    _0xef97x74['onerror'] = function() {
        setProgress(0, 'XHR error.');
        doneProcessing()
    };
    _0xef97x74['upload']['onprogress'] = function(_0xef97x7) {
        if (_0xef97x7['lengthComputable']) {
            var _0xef97x2a = Math['round']((_0xef97x7['loaded'] / _0xef97x7['total']) * 100);
            setProgress(_0xef97x2a, _0xef97x2a == 100 ? 'Finalizing.' : 'Uploading.')
        }
    };
    _0xef97x74['onreadystatechange'] = function(_0xef97x7) {
        if (_0xef97x74['readyState'] == 1) {
            g_statusText['innerHTML'] += '<br>Connecting...'
        };
        if (_0xef97x74['readyState'] == 2) {
            g_statusText['innerHTML'] += '<br>Uploading...'
        };
        if (_0xef97x74['readyState'] == 4) {
            g_statusText['innerHTML'] += '<br>upload done....'
        }
    };
    _0xef97x74 = openCORSRequest(_0xef97x74, 'PUT', _0xef97xb['puturl']);
    _0xef97x74['setRequestHeader']('Content-Type', 'application/wef');
    _0xef97x74['setRequestHeader']('x-amz-acl', 'public-read');
    g_xhttp = _0xef97x74;
    _0xef97x74['send'](_0xef97x18);
    return false
}
var email_store = null;

function sendEmail() {
    var _0xef97x77 = document['getElementById']('msgformat')['value'];
    if (_0xef97x77 == 'link') {
        DoEncrypt(true, true, 'gmail')
    } else {
        if (_0xef97x77 == 'html') {
            DoEncrypt(false, true, 'html')
        } else {
            DoEncrypt(false, true, 'wef')
        }
    }
}

function DoEncrypt(_0xef97x79, _0xef97x7a, _0xef97x7b) {
    var _0xef97xf = document['getElementById']('value');
    var _0xef97x59;
    _0xef97x59 = tinyMCE['activeEditor']['getContent']();
    _0xef97x59 = insertVoice(_0xef97x59);
    InitProgress('Lockmagic');
    g_statusText = document['getElementById']('statusmsg');
    var _0xef97x5d = new Object();
    _0xef97x5d['iss3'] = _0xef97x79;
    _0xef97x5d['isemail'] = _0xef97x7a;
    _0xef97x5d['cloudstore'] = _0xef97x7b;
    _0xef97x5d['isviewer'] = false;
    if (document['getElementById']('viewer')['checked']) {
        _0xef97x5d['isviewer'] = true
    };
    var _0xef97x5a = document['getElementById']('aclinput')['value'];
    if (_0xef97x5a != null) {
        _0xef97x5a = _0xef97x5a['split'](' ')['join']('');
        _0xef97x5a = _0xef97x5a['replace'](/,/g, ';');
        if (verifyemail(_0xef97x5a) != null) {
            g_statusText['innerHTML'] = 'Invalid email: ' + _0xef97x5a;
            return false
        }
    };
    var _0xef97x5b = document['getElementById']('ccaclinput')['value'];
    if (_0xef97x5b != null) {
        _0xef97x5b = _0xef97x5b['split'](' ')['join']('');
        _0xef97x5b = _0xef97x5b['replace'](/,/g, ';');
        if (verifyemail(_0xef97x5b) != null) {
            g_statusText['innerHTML'] = 'Invalid email: ' + _0xef97x5b;
            return false
        }
    };
    var _0xef97x5c = document['getElementById']('bccaclinput')['value'];
    if (_0xef97x5c != null) {
        _0xef97x5c = _0xef97x5c['split'](' ')['join']('');
        _0xef97x5c = _0xef97x5c['replace'](/,/g, ';');
        if (verifyemail(_0xef97x5c) != null) {
            g_statusText['innerHTML'] = 'Invalid email: ' + _0xef97x5c;
            return false
        }
    };
    if (_0xef97x5a['length'] == 0 && _0xef97x5b['length'] == 0 && _0xef97x5c['length'] == 0) {
        g_statusText['innerHTML'] = 'Enter email address to send to.';
        return false
    };
    var _0xef97x20 = new Array();
    var _0xef97x6a = document['getElementById']('fileinput')['files'];
    for (var _0xef97x7c = 0; _0xef97x7c < _0xef97x6a['length']; _0xef97x7c++) {
        attachments['push'](_0xef97x6a[_0xef97x7c])
    };
    _0xef97x6a = attachments;
    if (_0xef97x6a['length'] == 0) {
        CreateEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, null, _0xef97x5d);
        return true
    };
    for (var _0xef97x7c = 0; _0xef97x7c < _0xef97x6a['length']; _0xef97x7c++) {
        LoadFileForEml(_0xef97x59, _0xef97x5a, _0xef97x5b, _0xef97x5c, _0xef97x6a[_0xef97x7c], _0xef97x20, _0xef97x7c, _0xef97x6a['length'], _0xef97x5d)
    };
    return true
}

function pDoEncryptBufferArray(_0xef97x5a, _0xef97x26, _0xef97x61, _0xef97x18, _0xef97x7e, _0xef97x7a, _0xef97x79, _0xef97x7b, _0xef97x7f) {
    var _0xef97x80 = parseInt(expiryValue);
    if (_0xef97x80 != 0) {
        var _0xef97x81 = new Date();
        _0xef97x81['setTime'](_0xef97x81['getTime']() + (_0xef97x80 * 1000));
        _0xef97x80 = _0xef97x81['toUTCString']()
    };
    hideDiv('actiontable');
    hideDiv('progress_bar');
    g_statusText = document['getElementById']('statusmsg');
    var _0xef97x82 = document['getElementById']('guest')['checked'];
    if (_0xef97x82) {
        _0xef97x5a += ';guest@lockmagic.com'
    };
    if (_0xef97x7e) {
        _0xef97x5a = appendrole(_0xef97x5a, ':V')
    } else {
        _0xef97x5a = appendrole(_0xef97x5a, ':E')
    };
    var _0xef97x83 = _0xef97x61;
    if (_0xef97x83['length'] < 1) {
        _0xef97x83 = '[Secure]'
    };
    if (_0xef97x82 == false) {
        setcookie('mylockmagicusers', _0xef97x5a['replace'](/guest@lockmagic.com/g, ''), 2592000000, '/')
    };
    var _0xef97x5d = new Object();
    if (lastwriter != null) {
        _0xef97x5d['author'] = lastwriter
    };
    _0xef97x5d['expiry'] = _0xef97x80;
    _0xef97x5d['store'] = (_0xef97x79 == true) ? 's3' : null;
    _0xef97x5d['subject'] = _0xef97x83;
    _0xef97x5d['appname'] = 'gmail';
    showImg('statusmsg');
    g_statusText['innerHTML'] += 'Contacting key server.....';
    var _0xef97x84 = createPostCallbackHandlerObject(_0xef97x5a, _0xef97x26, _0xef97x83, _0xef97x82, _0xef97x7a, _0xef97x79, _0xef97x7b, _0xef97x7f);
    LockMagicEncryptArrayBuffer(_0xef97x5a, _0xef97x26, _0xef97x18, _0xef97x84, _0xef97x5d)
}

function createPostCallbackHandlerObject(_0xef97x5a, _0xef97x26, _0xef97x83, _0xef97x82, _0xef97x7a, _0xef97x79, _0xef97x7b, _0xef97x86) {
    if (_0xef97x7a == false && _0xef97x79 == false && _0xef97x7b == null) {
        return null
    };
    var _0xef97x84 = new Object();
    _0xef97x84['handler'] = WrapHtmlEmailMessage;
    _0xef97x84['users'] = _0xef97x5a;
    _0xef97x84['filename'] = _0xef97x26;
    _0xef97x84['subject'] = _0xef97x83;
    _0xef97x84['geturl'] = null;
    _0xef97x84['puturl'] = null;
    _0xef97x84['getshorturl'] = null;
    _0xef97x84['storename'] = _0xef97x7b;
    _0xef97x84['guestallowed'] = _0xef97x82;
    _0xef97x84['metadata'] = _0xef97x86;
    if (_0xef97x7b == 'gmail' || _0xef97x7b == 'hotmail') {
        _0xef97x84['handler'] = sendLinkHandler
    };
    if (_0xef97x7a == true && _0xef97x79 == false) {
        if (_0xef97x84['metadata'] == null) {
            var _0xef97x87 = striprole(_0xef97x5a);
            _0xef97x87 = _0xef97x87['replace'](/;/g, ',');
            _0xef97x87 = _0xef97x87['replace'](/guest@lockmagic.com/g, '');
            var _0xef97x21 = new Object();
            _0xef97x21['from'] = getFromEmail(lockmagic_currentuser, '@gmail.com');
            _0xef97x21['to'] = _0xef97x87;
            _0xef97x21['cc'] = null;
            _0xef97x21['bcc'] = null;
            _0xef97x21['subject'] = _0xef97x83;
            _0xef97x84['metadata'] = _0xef97x21
        };
        _0xef97x84['attachname'] = _0xef97x26;
        _0xef97x84['callback'] = gmailSend
    };
    return _0xef97x84
}

function getFromEmail(_0xef97x89, _0xef97x8a) {
    var _0xef97x20 = _0xef97x89['split'](';');
    var _0xef97x7c = 0;
    for (_0xef97x7c = 0; _0xef97x7c < _0xef97x20['length']; _0xef97x7c++) {
        if (_0xef97x20[_0xef97x7c]['indexOf'](_0xef97x8a) > 0) {
            return _0xef97x20[_0xef97x7c]
        }
    };
    return _0xef97x20[0]
}

function showExpiry(_0xef97x8c) {
    g_statusText = document['getElementById']('xstatusmsg');
    var _0xef97x80 = document['getElementById']('expiry')['value'];
    if (_0xef97x80 != 0) {
        var _0xef97x81 = new Date();
        _0xef97x81['setTime'](_0xef97x81['getTime']() + (_0xef97x80 * 1000));
        _0xef97x80 = _0xef97x81['toUTCString']();
        g_statusText['innerHTML'] = 'Content expires on ' + _0xef97x80
    } else {
        g_statusText['innerHTML'] = 'Content never expires'
    }
}

function startApp() {
    if ((navigator['userAgent']['match'](/iPhone/i)) || (navigator['userAgent']['match'](/iPad/i))) {
        var _0xef97x8e = getParam('a');
        if (_0xef97x8e != null && _0xef97x8e != '') {
            lastwriter = _0xef97x8e;
            lockmagic_currentuser = _0xef97x8e;
            signinDone(_0xef97x8e, null);
            return
        }
    };
    lockmagic_getusername(signinDone)
}

function signinDone(_0xef97x90, _0xef97x91) {
    initTinyEditor('');
    var _0xef97x92 = false;
    var _0xef97x93 = false;
    if (_0xef97x90 != null && _0xef97x90['length'] > 0) {
        var _0xef97x94 = OnLoad(_0xef97x90);
        var _0xef97x95 = document['getElementById']('aclinput');
        if (_0xef97x95) {
            var _0xef97x96 = getParam('u');
            var _0xef97x97 = getParam('to');
            if (_0xef97x97 != null && _0xef97x97 != '') {
                if (_0xef97x96 != null && _0xef97x96['length'] > 0) {
                    var _0xef97x98 = _0xef97x97['replace'](/,/g, ';')['split'](';');
                    var _0xef97x99 = [];
                    for (var _0xef97x7c = 0; _0xef97x7c < _0xef97x98['length']; _0xef97x7c++) {
                        if (_0xef97x98[_0xef97x7c] != _0xef97x90) {
                            _0xef97x99[_0xef97x99['length']] = _0xef97x98[_0xef97x7c]
                        }
                    };
                    _0xef97x97 = _0xef97x99['join'](';');
                    _0xef97x97 = _0xef97x96 + (_0xef97x97['length'] > 0 ? ';' + _0xef97x97 : '')
                };
                _0xef97x96 = _0xef97x97
            } else {
                _0xef97x97 = _0xef97x96
            };
            var _0xef97x87 = _0xef97x91;
            if (_0xef97x87 == null || _0xef97x87['length'] == 0) {
                _0xef97x87 = getcookie('mylockmagicusers');
                if (_0xef97x87 != null && _0xef97x87['length'] > 0) {
                    _0xef97x87 = striprole(_0xef97x87)
                }
            };
            if (_0xef97x96 != null && _0xef97x96['length'] > 0) {
                _0xef97x97 = unescape(_0xef97x96)
            };
            var _0xef97x5e = new Object();
            if (_0xef97x97 != null && _0xef97x97['length'] > 0) {
                _0xef97x97 = _0xef97x97['replace'](/,/g, ';');
                _0xef97x95['value'] = _0xef97x97;
                _0xef97x95['readOnly'] = true;
                if (is_ios_mobile() == true) {
                    _0xef97x95['style']['display'] = 'none';
                    var _0xef97x9a = document['getElementById']('totr');
                    if (_0xef97x9a) {
                        _0xef97x9a['style']['display'] = 'none'
                    };
                    _0xef97x9a = document['getElementById']('totrs');
                    if (_0xef97x9a) {
                        _0xef97x9a['style']['display'] = 'none'
                    };
                    document['getElementById']('subject')['style']['display'] = 'none'
                };
                var _0xef97x9b = document['getElementById']('ccaclinput');
                if (_0xef97x9b) {
                    _0xef97x9b['readOnly'] = true;
                    _0xef97x92 = true
                };
                var _0xef97x9c = document['getElementById']('bccaclinput');
                if (_0xef97x9c) {
                    _0xef97x9c['readOnly'] = true;
                    _0xef97x93 = true
                }
            } else {
                _0xef97x87 = _0xef97x87['replace'](/,/g, ';');
                _0xef97x5e['placeholder'] = 'To';
                _0xef97x5e['tokenSeparators'] = [',', ' ', ';', '\x0A', '	'];
                _0xef97x5e['tags'] = (_0xef97x87 && _0xef97x87['length'] > 0) ? _0xef97x87['split'](';') : [];
                $('#aclinput')['select2'](_0xef97x5e)
            }
        };
        var _0xef97x9b = document['getElementById']('ccaclinput');
        if (_0xef97x9b) {
            var _0xef97x9d = getParam('cc');
            var _0xef97x9e = _0xef97x91;
            if (_0xef97x9e == null || _0xef97x9e['length'] == 0) {
                _0xef97x9e = getcookie('mylockmagicusers');
                if (_0xef97x9e != null && _0xef97x9e['length'] > 0) {
                    _0xef97x9e = striprole(_0xef97x9e)
                }
            };
            if (_0xef97x9d != null && _0xef97x9d != '') {
                _0xef97x9e = unescape(_0xef97x9d)
            };
            var _0xef97x5e = new Object();
            if (_0xef97x9d != null && _0xef97x9d['length'] > 0) {
                _0xef97x9d = _0xef97x9d['replace'](/,/g, ';');
                _0xef97x9b['value'] = _0xef97x9d;
                _0xef97x9b['readOnly'] = true;
                _0xef97x92 = false
            } else {
                _0xef97x9e = _0xef97x9e['replace'](/,/g, ';');
                _0xef97x5e['placeholder'] = 'Cc';
                _0xef97x5e['tokenSeparators'] = [',', ' ', ';', '\x0A', '	'];
                _0xef97x5e['tags'] = (_0xef97x9e && _0xef97x9e['length'] > 0) ? _0xef97x9e['split'](';') : [];
                $('#ccaclinput')['select2'](_0xef97x5e)
            };
            if (_0xef97x92 == true) {
                var _0xef97x9a = document['getElementById']('cctr');
                if (_0xef97x9a) {
                    _0xef97x9a['style']['display'] = 'none'
                };
                _0xef97x9a = document['getElementById']('cctrs');
                if (_0xef97x9a) {
                    _0xef97x9a['style']['display'] = 'none'
                }
            }
        };
        var _0xef97x9c = document['getElementById']('bccaclinput');
        if (_0xef97x9c) {
            var _0xef97x9d = getParam('bcc');
            var _0xef97x9e = _0xef97x91;
            if (_0xef97x9e == null || _0xef97x9e['length'] == 0) {
                _0xef97x9e = getcookie('mylockmagicusers');
                if (_0xef97x9e != null && _0xef97x9e['length'] > 0) {
                    _0xef97x9e = striprole(_0xef97x9e)
                }
            };
            if (_0xef97x9d != null && _0xef97x9d != '') {
                _0xef97x9e = unescape(_0xef97x9d)
            };
            var _0xef97x5e = new Object();
            if (_0xef97x9d != null && _0xef97x9d['length'] > 0) {
                _0xef97x9d = _0xef97x9d['replace'](/,/g, ';');
                _0xef97x9c['value'] = _0xef97x9d;
                _0xef97x9c['readOnly'] = true;
                _0xef97x93 = false
            } else {
                _0xef97x9e = _0xef97x9e['replace'](/,/g, ';');
                _0xef97x5e['placeholder'] = 'Bcc';
                _0xef97x5e['tokenSeparators'] = [',', ' ', ';', '\x0A', '	'];
                _0xef97x5e['tags'] = (_0xef97x9e && _0xef97x9e['length'] > 0) ? _0xef97x9e['split'](';') : [];
                $('#bccaclinput')['select2'](_0xef97x5e)
            };
            if (_0xef97x93 == true) {
                var _0xef97x9a = document['getElementById']('bcctr');
                if (_0xef97x9a) {
                    _0xef97x9a['style']['display'] = 'none'
                };
                _0xef97x9a = document['getElementById']('bcctrs');
                if (_0xef97x9a) {
                    _0xef97x9a['style']['display'] = 'none'
                }
            }
        };
        var _0xef97x96 = document['getElementById']('userid');
        if (_0xef97x96) {
            _0xef97x96['innerHTML'] = '<a href=\'javascript:LmContacts();\'><b>' + _0xef97x90 + '</b></a>'
        };
        OnLoad(_0xef97x90);
        var _0xef97x54 = location['href'];
        var _0xef97x9f = _0xef97x54['indexOf']('#');
        _0xef97x54 = _0xef97x54['substring'](0, _0xef97x9f);
        window['history']['pushState'](null, 'LockMagic Secure GMail Messaging', _0xef97x54);
        initDrop('main')
    }
}

function OnLoad(_0xef97x90) {
    email_store = getParam('em');
    var _0xef97x96 = getParam('u');
    var _0xef97x61 = getParam('s');
    if (_0xef97x61 != null && _0xef97x61 != '') {
        var _0xef97xa1 = document['getElementById']('subject');
        _0xef97x61 = unescape(_0xef97x61);
        if (_0xef97xa1 != null) {
            var _0xef97xa2 = _0xef97x61['toLowerCase']();
            while (_0xef97xa2['startsWith']('re: ')) {
                _0xef97xa2 = _0xef97xa2['substring'](4);
                _0xef97x61 = _0xef97x61['substring'](4)
            };
            _0xef97xa1['value'] = initsubjecttext = 'RE: ' + _0xef97x61
        }
    };
    var _0xef97xa3 = getParam('b');
    if (_0xef97xa3 != null && _0xef97xa3 != '') {
        var _0xef97xa1 = document['getElementById']('value');
        if (_0xef97xa1 != null) {
            var _0xef97xa4 = _0xef97x96;
            var _0xef97x87 = getParam('t');
            var _0xef97xa5 = getParam('d');
            if (_0xef97xa5) {
                var _0xef97xa6 = new Date();
                _0xef97xa6['setTime'](_0xef97xa5);
                _0xef97xa5 = '' + _0xef97xa6
            };
            var _0xef97xa7 = '&nbsp;<br/><br/><hr>';
            if (_0xef97xa4) {
                _0xef97xa7 += '<b>From:</b>&nbsp;' + _0xef97xa4
            };
            if (_0xef97x87) {
                _0xef97xa7 += '<br/><b>To:</b>&nbsp;' + _0xef97x87
            };
            if (_0xef97xa5) {
                _0xef97xa7 += '<br/><b>Sent:</b>&nbsp;' + _0xef97xa5
            };
            if (_0xef97x61) {
                _0xef97xa7 += '<br/><b>Subject:</b>&nbsp;' + _0xef97x61
            };
            _0xef97xa7 += '<br/><br/>';
            _0xef97xa3 = _0xef97xa7 + atob(_0xef97xa3);
            _0xef97xa1['innerHTML'] = _0xef97xa3
        }
    };
    _0xef97xa3 = getParam('rb');
    if (_0xef97xa3 != null && _0xef97xa3 != '') {
        var _0xef97xa1 = document['getElementById']('value');
        if (_0xef97xa1 != null) {
            var _0xef97xa7 = '&nbsp;<br/><br/>';
            _0xef97xa7 += '<br/><br/>';
            _0xef97xa3 = _0xef97xa7 + atob(_0xef97xa3);
            _0xef97xa1['innerHTML'] = _0xef97xa3;
            var _0xef97x95 = document['getElementById']('aclinput');
            if (_0xef97x95) {
                _0xef97x95['readOnly'] = false
            }
        }
    };
    return _0xef97x96
}

function Find_onclick() {
    var _0xef97x32 = document['getElementById']('name');
    var _0xef97x51 = '../contacts.aspx?search=1&name=' + _0xef97x32['value'];
    document['getElementById']('DivCT')['innerHTML'] = '<iframe src=\'' + _0xef97x51 + '\' width=365 height=200></iframe>'
}

function listContacts() {
    var _0xef97x51 = '../contacts.aspx';
    document['getElementById']('DivCT')['innerHTML'] = '<iframe src=\'' + _0xef97x51 + '\' width=365 height=200></iframe>'
}

function getParam(_file_name) {
    _file_name = _file_name['replace'](/[\[]/, '\[')['replace'](/[\]]/, '\]');
    var _0xef97xab = '[\?&]' + _file_name + '=([^&#]*)';
    var _0xef97xac = new RegExp(_0xef97xab);
    var _0xef97x51 = window['location']['href']['replace']('#', '?');
    var _0xef97xad = _0xef97xac['exec'](_0xef97x51);
    if (_0xef97xad == null) {
        return ''
    } else {
        return _0xef97xad[1]
    }
}
var lastname = null;
var initsubjecttext = '';

function AddUsers_onclick() {
    processList(true)
}

function processList(_0xef97xb2) {
    var _0xef97x9f = getParam('n');
    if (_0xef97x9f == null || _0xef97x9f == '') {
        _0xef97x9f = 'aclinput'
    };
    if (lastname != null) {
        _0xef97x9f = lastname
    };
    var _0xef97xb3 = document['getElementById'](_0xef97x9f);
    if (_0xef97xb3['readOnly'] == true) {
        if (_0xef97xb2 == true) {
            alert('You cannot add users to this message')
        };
        return
    };
    var _0xef97x7c = 0;
    var _0xef97x32;
    var _0xef97xb4 = '';
    var _0xef97xb5 = 0;
    var _0xef97xb6 = _0xef97xb3['value'];
    var _0xef97xb7 = window['frames'][0]['document']['getElementById']('ContactTab')['rows']['length'];
    while (_0xef97x7c < _0xef97xb7) {
        _0xef97x32 = window['frames'][0]['document']['getElementById']('Checkbox' + _0xef97x7c);
        if (_0xef97x32 && _0xef97x32['checked'] == true) {
            var _0xef97xb8 = _0xef97x32;
            _0xef97x32['enabled'] = false;
            _0xef97x32 = window['frames'][0]['document']['getElementById']('email' + _0xef97x7c);
            if (_0xef97xb6 == null || _0xef97xb6 == '' || _0xef97xb6['indexOf'](_0xef97x32['innerHTML']) == -1) {
                if (_0xef97xb5 > 0) {
                    _0xef97xb4 += ';'
                };
                _0xef97xb4 += _0xef97x32['innerHTML'];
                _0xef97xb5++
            } else {
                if (_0xef97xb2 == true && _0xef97xb8['disabled'] != 'disabled') {
                    alert(_0xef97x32['innerHTML'] + ' is already added')
                }
            };
            _0xef97xb8['checked'] = false;
            _0xef97xb8['disabled'] = 'disabled';
            _0xef97xb8['style']['visibility'] = 'hidden'
        };
        _0xef97x7c++
    };
    if (_0xef97xb4 != '') {
        _0xef97xb6 = _0xef97xb3['value'];
        if (_0xef97xb6 != null && _0xef97xb6 != '') {
            _0xef97xb4 = ';' + _0xef97xb4
        };
        _0xef97xb3['value'] += _0xef97xb4
    }
}

function showHide(_0xef97xba) {
    var _0xef97xbb = document['getElementById'](_0xef97xba);
    if (_0xef97xbb['style']['display'] == 'none') {
        _0xef97xbb['style']['display'] = ''
    } else {
        _0xef97xbb['style']['display'] = 'none'
    };
    showcc['style']['display'] = 'none'
}

function setFont() {
    var _0xef97xbd = document['getElementById']('fontlist');
    var _0xef97xbe = _0xef97xbd['options'][_0xef97xbd['selectedIndex']]['value'];
    _0xef97xbd = document['getElementById']('value');
    _0xef97xbd['style']['fontFamily'] = _0xef97xbe
}

function setFontSize() {
    var _0xef97xbd = document['getElementById']('fontsizelist');
    var _0xef97xbe = _0xef97xbd['options'][_0xef97xbd['selectedIndex']]['value'];
    _0xef97xbd = document['getElementById']('value');
    _0xef97xbd['style']['fontSize'] = _0xef97xbe
}
var initsubject = 1;

function cleartext(_0xef97xba, _0xef97xc2) {
    var _0xef97xbb = document['getElementById'](_0xef97xba);
    if (initsubject == 1) {
        _0xef97xbb['value'] = initsubjecttext;
        _0xef97xbb['style']['color'] = 'black'
    };
    initsubject = 0
}

function () {
    document['getElementById']('attachsection')['style']['display'] = 'block';
    document['getElementById']('fileinput')['click']()
}

function OpenHelpWindow(_0xef97xc5) {
    _0xef97xc5 = 'lmeml/popups/about_' + _0xef97xc5 + '.html';
    window['open'](_0xef97xc5, 'popup', 'location=0,status=0,scrollbars=1,resizable=1,width=400,height=350,top=' + 29 + ',left=' + 20)['focus']()
}

function guestChange(_0xef97x9a) {
    g_statusText = document['getElementById']('xstatusmsg');
    g_statusText['innerHTML'] = '';
    var _0xef97xc7 = document['getElementById'](_0xef97x9a);
    if (_0xef97xc7['checked']) {
        var _0xef97x80 = document['getElementById']('expiry');
        if (_0xef97x80['value'] == 0 || _0xef97x80['selectedIndex'] > 5) {
            _0xef97x80['selectedIndex'] = 5;
            g_statusText['innerHTML'] = 'We recommend setting a very short time expiry when guest access is enabled'
        }
    }
}

function viewerChange(_0xef97x9a) {}

function msgformatChanged() {
    var _0xef97xc7 = document['getElementById']('msgformat')['value'];
    g_statusText = document['getElementById']('xstatusmsg');
    if (_0xef97xc7 == 'link') {
        g_statusText['innerHTML'] = 'The receipient receives the secure message as an attachment to open using a web browser.<br> In order to support mobile devices a copy of the secure message is stored in Amazon S3 with a link available for 10 days. <br><br>You can <a target=_default href=/ui/uipub.html>manage</a> published content folder @ https://www.lockmagic.com/ui/uipub.html'
    } else {
        if (_0xef97xc7 == 'wef') {
            g_statusText['innerHTML'] = 'The recipient must use Lockmagic client or lockmagic web app to open the message'
        } else {
            if (_0xef97xc7 == 'html') {
                g_statusText['innerHTML'] = 'The receipient receives the secure message as an attachment and opens it using a web browser.<br><br><i>For mobile friendly viewing we recommend using web universal format instead.</i>'
            }
        }
    }
}
