var appkey=null;
var reader=new FileReader();;
var files;
var NumSelectedFiles=0;
var Status={
  totalFiles:0,
  totalUnlocked:0,
  totalLocked:0
}
var token="0XcDTbH2tOAGBw%2fLMh9vZ6eutNUtU8LbPYxYmEVxCiyB%2f8vymEA%2fnJfwgKyF%2byJQoqdrsWBu2VXoJr5m7h91wmo9PxA%2bN5yKfXjB7ouSAr0RVoMz8vGMFwH9ky4z3cYJKDzlNoYOjEq6wzD7wfnovgNxAwqTD4aRGLmr8RrgMKMqvakOznpovg%2bDj4VpKQjwje3dosvewJuCgmXM5voLaYAVtJq0Cj2SDPvwSZCR%2fDxWDErw9ZYcrxxwCx1%2bD9KW0mCq8nnbJObDu9sDq90vxfpaSdRa4OngSvV8Tcubw6znU7oeHBRw%2bMrr0Ya1EKyDiwSxJfb4HPG4dOR8p61Nl8%2fylb0ibJnI%2bl8EJzndSKip91fFOB8UiBTXIQreDo83djF7%2bhmu7ZTehtoAEV97bw%3d%3d";

$(document).ready(function(){
  $("#file").change(fileuploadChange);
  appkey=new TheAppKey();
  appkey.Init({ usertoken: token });
});

var addFileToList=function(filename,fileID,islocked){
  var fileButton="";
  if(islocked){
    fileButton="<button type='button' id=unlock"+fileID+" class='btn btn-primary pull-right'>unLock</button>";
    return "<li id=li"+fileID+" class='list-group-item '>"+filename+fileButton+"</li>"
  }
  else {
    fileButton="<button type='button' id=lock"+fileID+" class='btn btn-primary pull-right'>lock</button>"
    return "<li id=li"+fileID+" class='list-group-item'>"+filename+fileButton+"</li>"
  }
}
var addFileToSuccessList=function(filename,fileID){
    return "<li class='list-group-item list-group-item-success'>"+filename+"<i class='icon-lock '></i> </li>"
}

var fileuploadChange=function(event){
  $("#lockFileList").empty()
  var lockedCount=0;
  files=event.target.files;
  Status.totalFiles=files.length
  var isLocked;
  for (var i = 0; i < files.length; i++) {
    var currentFile=files[i];
    isLocked=appkey.IsSecureFile(currentFile.name);
      if(isLocked){//change it to if current file is locked , you get it from the api
        //Status.totalLocked++;
        $("#lockFileList").append(addFileToList(currentFile.name,i,true))
        $("#unlock"+i).click(fileUnlockHandler);
      }else {
        //Status.totalUnlocked++;
        $("#lockFileList").append(addFileToList(currentFile.name,i,false))
        $("#lock"+i).click(fileLockHandler);
      }
  }
  $("#fileUploadAlert").text(Status.totalFiles+" total, "+Status.totalUnlocked+" unlocked,"+Status.totalLocked+" locked")
  return false;
};

var fileLockHandler=function(event){
  fileID=Number(event.target.id.replace("lock",""));
  //reader.readAsArrayBuffer(files[fileID])
  //reader.onload=function(event){
  var expiry = null;
  if (true) {
      var date = new Date();
      date.setTime(date.getTime() + (604800 * 1000));
      expiry = date.toUTCString();
  }
    var options = {
                  accesstoken: token,
                  data: files[fileID],
                  users: [
                      { email: "mmusa71@gmail.com", role: "Editor" },
                  ],
                  expiry: expiry,
                  success: function(result){
                    Status.totalLocked++;
                    $("#fileUploadAlert").text("Success: "+Status.totalUnlocked+" unlocked, "+Status.totalLocked+" locked")
                    $("#lock"+fileID).hide()
                    //$("#li"+fileID).addClass("list-group-item-success");
                    $("#lockFileList #li"+fileID).remove()
                    $("#lockFileListSuccess").append(addFileToSuccessList(files[fileID].name,fileID));
                  },
                  progress: function (e) {
                    $("#fileUploadAlert").text("Encrypting...")
                  },
                  error: function (e) {
                      alert("Encrypt failed with error: " + e.status + " " + e.message);
                  }
              };
              appkey.Encrypt(options);
  //}

};
var DecryptCallbackHandler=function(e){

};

var fileUnlockHandler=function(){
  fileID=Number(event.target.id.replace("unlock",""));
  reader.readAsArrayBuffer(files[fileID])
  reader.onload=function(event){
    var options = {
                      accesstoken: token,
                      data: event.target.result,
                      success: function(){
                        Status.totalUnlocked++;
                        $("#fileUploadAlert").text("Success: "+Status.totalUnlocked+" unlocked, "+Status.totalLocked+" locked")
                        $("#lockFileList #li"+fileID).remove()
                        $("#lockFileListSuccess").append(addFileToSuccessList(files[fileID].name,fileID));
                      },
                      progress: function (e) {
                        $("#fileUploadAlert").text("Decrypting...")
                      },
                      error: function (e) {
                          alert("Decrypt failed with error: " + e.status + " " + e.message);
                      }
                  };
                  appkey.Decrypt(options); // call decrypt method
              };

};
